# Calculations Behind the screen

I am now writing some examples for explain how the calculations work in terms of when the user is using the tool to 
check the graph and get the result from local currency to foreign currency or from foreign currency to local currency.

## Local currency to foreign currencies

I am picking an example with the screen capture underneath. In this case we get the exchange rates of: 

The starting of this graph (May 2017): 1.2896
The lowest point of this graph (Jun 2017): 1.263200
The highest point of the graph (May 2018): 1.43405
The end of this graph: (May 2018): 1.37723

At the lowest point of this graph. If a person bought $1,000,000 USD with GBP, he would pay the highest price compare  
to the others who bought USD at any other days.

791,640.28 is the amount he would pay in GBP: 1000000 / 1.263200

In case the person bought $1,000,000 USD with GBP at the day which gets the highest exchange rate:

697,325.76 is the amount he would pay in GBP: 1000000 / 1.43405

At the right hand side of the graph we get 775,434.24, that is the price he would need to pay when he bought it at the 
start day of this graph: 1000000 / 1.2896


## Foreign currencies to Local currency

In the example 2 here we get the following graph which has these exchange rates behind: 

The starting of this graph (May 2017): 0.7727
The lowest point of this graph (Jun 2017): 0.69729
The highest point of the graph (May 2018): 0.7916
The end of this graph: (May 2018): 0.72605

At the lowest point of this graph. If a person bought GBP with $1,000,000 USD, he would pay the highest price compare  
to the others who bought USD at any other days.

£755,996,22 is the amount he would pay in GBP: 726050 * (0.72605 / 0.69729)

In case the person bought $1,000,000 USD with GBP at the day which gets the highest exchange rate:

£665,928.00 is the amount he would pay in GBP: 726050 * (0.72605 / 0.7916)

At the right hand side of the graph we get 772,700.00, that is the price he would need to pay if he bought it at the 
start day of this graph: 1000000 * 0.7727
