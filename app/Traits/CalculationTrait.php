<?php

namespace App\Traits;

use App\ExchangeData;

trait CalculationTrait
{

    static function calculateGraphData($options, &$fluctuationGain = null) {

        // params
        $options = array_merge([
            'from' => null,
            'to' => null,
            'chartEndDate' => null,
            'chartStartDate' => null,
            'rateReference' => null,
            'totalAmount' => null,
            'chartEndTime' => time(), // default is now
            'minMax' => false, // default is false
        ], $options);

        $rateReference = $options['rateReference'];
        $from = $options['from'];
        $to = $options['to'];
        $symbol = "{$from}{$to}";
        $chartEndDate = $options['chartEndDate'];
        $chartStartDate = $options['chartStartDate'];
        $totalAmount = $options['totalAmount'];
        $calculatingPairs = config('amex.currencies.calculatingPairs');
        $calculationFrom = isset($calculatingPairs[$symbol]) ? $calculatingPairs[$symbol] : null;
        $needsCalculation = !is_null($calculationFrom);

        // a essential dates in different months
        $essentialDates = array_map(function($item) {

            $exchangeData = ExchangeData
                ::select(['datetime'])
                ->where('datetime', '<=', date('Y-m-d H:i:s', $item))
                ->orderBy('datetime', 'DESC')
                ->first();

            return !is_null($exchangeData) ? substr($exchangeData['datetime'], 0, 10) : null;

        }, self::getEveryMonthDay($chartStartDate, $chartEndDate));

        $firstEssentialDates = isset($essentialDates[0]) ? $essentialDates[0] : null;

        // exchange datas
        $exchangeDatasCollection = ExchangeData::select([
            $rateReference,
            'datetime',
        ])
            ->where('datetime', '>=', "{$firstEssentialDates}")
            ->where('datetime', '<=', "{$chartEndDate}")
            ->where(['name' => !$needsCalculation ? "{$from}{$to}" : $calculationFrom])
            ->orderBy('datetime', 'ASC')
            ->get();

        if($exchangeDatasCollection->isEmpty()) {
            return null;
        }

        $exchangeDatas = $exchangeDatasCollection->toArray();

        if ($needsCalculation) {
            for($i = 0; $i < count($exchangeDatas); $i++) {
                $exchangeDatas[$i][$rateReference] = 1 / $exchangeDatas[$i][$rateReference];
            }
        }

        $startRate = null;
        $maxGain = -INF;
        $minGain = INF;

        for($i = 0; $i < count($exchangeDatas); $i++) {

            $unixtime = strtotime($exchangeDatas[$i]['datetime']);
            $datetime = $exchangeDatas[$i]['datetime'];
            $gain = null;
            $exchangeDatas[$i]['unixtime'] = $unixtime;
            $exchangeDatas[$i]['date'] = date('Y-m-d', $unixtime);

            $exchangeDatas[$i]['isEssentialDate'] = in_array(substr($datetime, 0, 10), $essentialDates);

            $exchangeDatas[$i]['amount'] = $exchangeDatas[$i][$rateReference] * $totalAmount;

            // if is first round
            if(is_null($startRate)) {
                $startRate = $exchangeDatas[$i][$rateReference];
                $gain = 0;
            } else {
                // calculate the change of the rate
                $currentRate = $exchangeDatas[$i][$rateReference];
                $gain = ($currentRate - $startRate) / $startRate;
            }

            if ($gain > $maxGain) {
                $maxGain = $gain;
            }
            if($gain < $minGain) {
                $minGain = $gain;
            }

            $exchangeDatas[$i]['gain'] = $gain;

        }

        $fluctuationGain = $maxGain - $minGain;

        return $exchangeDatas;
    }


    public static function getCurrentExchangerates() {
        $rateReference = config('amex.settings.rateReference');
        $availablePairs = config('amex.currencies.availablePairs');
        $result = [];
        foreach($availablePairs as $symbol) {
            $calculatingPairs = config('amex.currencies.calculatingPairs');
            $calculationFrom = isset($calculatingPairs[$symbol]) ? $calculatingPairs[$symbol] : null;
            $needsCalculation = !is_null($calculationFrom);
            $rate = ExchangeData::select([$rateReference])->where(['name' => !$needsCalculation ? $symbol : $calculationFrom])->orderBy('datetime', 'DESC')->firstOrFail()->$rateReference;
            $result[$symbol] = $needsCalculation ? 1 / $rate : $rate;
        }
        return $result;
    }

}