<?php

namespace App\Traits;

trait ApiTrait {

    public static function validateRequiredErrorMessage($fieldName) {
        return "{$fieldName} is required";
    }

    public static function validateInErrorMessage($fieldName, $inSet) {
        $str = json_encode($inSet);
        return "{$fieldName}'s value must be one of {$str}";
    }

    public static function validateNumericErrorMessage($fieldName) {
        return "{$fieldName}'s value must be numeric";
    }

    public static function validateBooleanErrorMessage($fieldName) {
        return "{$fieldName}'s value must be boolean";
    }

    public static function validateDifferentErrorMessage($fieldName1, $fieldName2) {
        return "{$fieldName1} and {$fieldName2} should not have the same value";
    }

    public static function responseJson($options) {
        return array_merge([
            'success' => false,
            'errorMessage' => null,
            'invalidFields' => new \stdClass(),
            'totalAmount' => null,
            'symbols' => null,
            'chartDates' => [],
            'dataHash' => null,
        ], $options);
    }

}
