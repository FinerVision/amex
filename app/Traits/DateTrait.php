<?php

namespace App\Traits;

trait DateTrait {

    static function getStartDate($totalMonth, $endDate) {
        return date('Y-m-d 00:00:00', strtotime("-{$totalMonth} months", strtotime($endDate)));
    }

    static function getEveryMonthDay($startDate, $endDate) {

        $output = [];
        $endDateUnix = strtotime($endDate);
        $currentDate = strtotime($startDate);

        do {
            $output[] = $currentDate;
            $currentDate = strtotime('+1 month', $currentDate);
        } while($currentDate < $endDateUnix);

        $output[] = $endDateUnix;

        return $output;
    }

}
