<?php

namespace App\Console\Commands;

use App\ExchangeData;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Sabre\Xml\Reader;

class FetchRealTimeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amex:fetch_real_time_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch the sample data from the netdania API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataType = 'real_time_quotes';
        $baseUri = config("amex.apiUrls.{$dataType}.base_uri");
        $path = config("amex.apiUrls.{$dataType}.path");
        $params = config("amex.apiUrls.{$dataType}.params");

        // build params symbol
        $allSymbols = config('amex.currencies.availablePairs');
        $unavailableSymbols = array_keys(config('amex.currencies.calculatingPairs'));
        $symbols = array_exclude($allSymbols, $unavailableSymbols);

        // dynamic parameters
        $params['symbols'] = implode('|', $symbols);
        $params['pass'] = env('NETDANIA_API_PASS');
        $params['fields'] = implode('|', array_values(config("amex.fieldNames.{$dataType}")));

        $this->info("Fetching real time data from API: {$baseUri}{$path}");

        // request
        $client = new Client([
            'base_uri' => $baseUri,
            'timeout' => 10.0,
        ]);

        $response = $client->get($path, [
            'query' => $params
        ]);

        $data = (string)$response->getBody();

        $xmlReader = new Reader();
        $xmlReader->xml($data);
        $result = $xmlReader->parse();
        $keys = config("amex.fieldNames.{$dataType}");

        // convert it to custom format
        $result = array_map(function($item) use ($keys) {

            $output = [];

            $output['open'] = $item['attributes']["f{$keys['open']}"];
            $output['high'] = $item['attributes']["f{$keys['high']}"];
            $output['low'] = $item['attributes']["f{$keys['close']}"];
            $output['close'] = $item['attributes']["f{$keys['low']}"];
            $output['name'] = str_replace('/', '', $item['attributes']["f{$keys['name']}"]);
            $output['datetime'] = $item['value'];

            return $output;

        }, $result['value']);

        foreach($result as $exchangeData) {
            $this->info("Importing data to database {$exchangeData['datetime']} {$exchangeData['name']}");

            $date = substr($exchangeData['datetime'], 0, 10);
            $duplicatedData = ExchangeData
                ::whereDate('datetime', $date)
                ->where(['name' => $exchangeData['name']])
                ->first();

            if (!is_null($duplicatedData)) {
                $this->info("Duplicated, skip");
                continue;
            }

            ExchangeData::create($exchangeData);
        }

    }
}
