<?php

namespace App\Console\Commands;

use App\ExchangeData;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Sabre\Xml\Reader;

class FetchHistoricalData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amex:fetch_historical_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dataType = 'historical_data';
        $baseUri = config("amex.apiUrls.{$dataType}.base_uri");
        $path = config("amex.apiUrls.{$dataType}.path");
        $params = config("amex.apiUrls.{$dataType}.params");


        // dynamic parameters
        $params['fields'] = implode('|', array_values(config("amex.fieldNames.{$dataType}")));
        $params['pass'] = env('NETDANIA_API_PASS');


        // build params symbol
        $symbols = config('amex.currencies.availablePairs');
        $unavailableSymbols = array_keys(config('amex.currencies.calculatingPairs'));

        foreach($symbols as $symbol) {

            if(in_array($symbol, $unavailableSymbols)) {
                continue;
            }

            $params['symbol'] = $symbol;

            // request
            $client = new Client([
                'base_uri' => $baseUri,
                'timeout' => 10.0,
            ]);

            $response = $client->get($path, [
                'query' => $params
            ]);

            $data = (string)$response->getBody();

            $xmlReader = new Reader();
            $xmlReader->xml($data);
            $result = $xmlReader->parse();
            $keys = config("amex.fieldNames.{$dataType}");

            // if the such exchange data is not provided
            if (is_null($result['value'])) {
                $this->warn("Warning: no data for exchange > {$symbol}");
                continue;
            }

            // convert it to custom format
            $result = array_map(function($item) use ($keys, $symbol) {

                $output = [];

                $output['open'] = $item['attributes']["f{$keys['open']}"];
                $output['high'] = $item['attributes']["f{$keys['high']}"];
                $output['low'] = $item['attributes']["f{$keys['close']}"];
                $output['close'] = $item['attributes']["f{$keys['low']}"];
                $output['name'] = $symbol;
                $output['datetime'] = $item['value'];

                return $output;

            }, $result['value']);

            foreach($result as $exchangeData) {
                $this->info("Importing data to database {$exchangeData['datetime']} {$exchangeData['name']}");
                ExchangeData::create($exchangeData);
                $this->info("done!");
            }

        }

    }
}
