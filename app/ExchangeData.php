<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeData extends Model
{
    protected $fillable = ['name', 'bid', 'ask', 'change', 'chanage_percentage', 'open', 'high', 'low', 'close', 'datetime'];
}
