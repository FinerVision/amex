<?php

function debugCache($path) {
    $version = env('APP_ENV', 'production') !== 'local' ? config('amex.version') : time();
    return "{$path}?v={$version}";
}

/**
 *
 * if there is a list of string, e.g. ['a', 'b', 'c'], the result of this method will be:
 *    ['ab', 'ac', 'ba', 'bc', 'ca', cc']
 *
 * @param $arr
 *
 * @return array
 */
function arrayAppendList($from, $to) {

    $output = [];

    foreach($from as $item1) {
        foreach($to as $item2) {
            if($item1 != $item2) {
                $output[] = "{$item1}{$item2}";
            }
        }
    }

    return $output;
}

/**
 * @param $originalSet
 * @param $excludingSet
 *
 * e.g.
 *    $originalSet = [1,2,3,4,5,6];
 *    $excludingSet = [2,3,4];
 *    array_exclude($originalSet, $excludingSet) >> [1,5,6]
 *
 */
if(!function_exists('array_exclude')) {
    function array_exclude($originalSet, $excludingSet) {
        return array_values(array_filter($originalSet, function($item) use ($excludingSet) {
            return !in_array($item, $excludingSet);
        }));
    }
}
