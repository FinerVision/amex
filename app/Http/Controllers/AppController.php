<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\DateTrait;
use App\Traits\ApiTrait;
use App\Traits\CalculationTrait;
use Illuminate\Validation\Rule;
use Validator;

class AppController extends Controller
{
    use DateTrait, ApiTrait, CalculationTrait;

    function index() {

        $props = [
            'baseUri' => env('APP_URL'),
            'title' => config('amex.settings.appTitle'),
            'currencies' => config('amex.currencies.details'),
            'baseCurrency' => config('amex.currencies.baseCurrency'),
            'from' => config('amex.currencies.from'),
            'to' => config('amex.currencies.to'),
            'text' => config('amex.text'),
            'lengthOfExposure' => config('amex.lengthOfExposure'),
            'settings' => config('amex.settings'),
            'currentExchangeRates' => self::getCurrentExchangerates(),
        ];

        return view('app', $props);
    }

    function api(Request $request) {

        $params = $request->all();
        $from = $request->get('from');
        $to = $request->get('to');
        $params['pair'] = "{$from}{$to}";

        // validation
        $validation = Validator::make($params, [
            'from' => [
                'required',
                Rule::in(config('amex.currencies.from')),
            ],
            'to' => [
                'required',
                Rule::in(config('amex.currencies.to')),
                'different:from',
            ],
            'pair' => [
                Rule::in(config('amex.currencies.availablePairs')),
            ],
            'totalAmount' => 'required|numeric',
            'lengthOfExposure' => [
                'required',
                Rule::in(array_column(config('amex.lengthOfExposure'), 'value')),
            ],
            'switchView' => 'boolean',
        ], [
            'from.required' => self::validateRequiredErrorMessage('from'),
            'from.in' => self::validateInErrorMessage('from', config('amex.currencies.from')),
            'to.required' => self::validateRequiredErrorMessage('to'),
            'to.in' => self::validateInErrorMessage('to', config('amex.currencies.to')),
            'to.different' => self::validateDifferentErrorMessage('value from', 'value to'),
            'pairs.in' => self::validateInErrorMessage('pairs', config('amex.currencies.availablePairs')),
            'totalAmount.required' => self::validateRequiredErrorMessage('total amount'),
            'totalAmount.numeric' => self::validateNumericErrorMessage('total amount'),
            'lengthOfExposure.required' => self::validateRequiredErrorMessage('length of exposure'),
            'lengthOfExposure.in' => self::validateInErrorMessage('length of exposure', array_column(config('amex.lengthOfExposure'), 'label')),
            'switchView.boolean' => self::validateNumericErrorMessage('switch view'),
        ]);

        // return error messages if fails
        if ($validation->fails()) {
            return self::responseJson([
                'errorMessage' => 'Form input is invalid',
                'invalidFields' => $validation->messages(),
            ]);
        }


        // config settings
        $rateReference = config('amex.settings.rateReference');

        // get params
        $totalAmount = $request->get('totalAmount');
        $lengthOfExposure = $request->get('lengthOfExposure');
        $switchView = $request->get('switchView');

        $exchangeDatas = null;

        if ($switchView) {

            $highestFluctuation = -INF;
            $highestFluctuationData = [
                'exchangeDatas' => null,
                'startMonth' => null,
            ];
            $monthsToCheck = config('amex.settings.highestVolatilitySwitch') - $lengthOfExposure + 1;
            $currentEndUnixTime = time();

            while($monthsToCheck-- > 0) {

                $chartEndDate = date('Y-m-d 00:00:00', $currentEndUnixTime); // now
                $chartStartDate = self::getStartDate($lengthOfExposure, $chartEndDate);
                $currentFluctuation = null;

                // simply fetch the graph which range contains length from {$lengthOfExposue} to now
                $exchangeDatas = self::calculateGraphData([
                    'from' => $from,
                    'to' => $to,
                    'chartEndDate' => $chartEndDate,
                    'chartStartDate' => $chartStartDate,
                    'lengthOfExposure' => $lengthOfExposure,
                    'rateReference' => $rateReference,
                    'totalAmount' => $totalAmount,
                ], $currentFluctuation);

                if ($currentFluctuation > $highestFluctuation) {
                    $highestFluctuation = $currentFluctuation;
                    $highestFluctuationData['startMonth'] = $chartStartDate;
                    $highestFluctuationData['exchangeDatas'] = $exchangeDatas;
                }

                // go 1 month backwards
                $currentEndUnixTime = strtotime('-1 month', $currentEndUnixTime);

            }

            $exchangeDatas = $highestFluctuationData['exchangeDatas'];

        } else {
            $chartEndDate = date('Y-m-d 00:00:00'); // now
            $chartStartDate = self::getStartDate($lengthOfExposure, $chartEndDate);

            // simply fetch the graph which range contains length from {$lengthOfExposue} to now
            $exchangeDatas = self::calculateGraphData([
                'from' => $from,
                'to' => $to,
                'chartEndDate' => $chartEndDate,
                'chartStartDate' => $chartStartDate,
                'lengthOfExposure' => $lengthOfExposure,
                'rateReference' => $rateReference,
                'totalAmount' => $totalAmount,
            ]);
        }

        return self::responseJson([
            'success' => true,
            'totalAmount' => $totalAmount,
            'symbols' => "{$from}{$to}",
            'chartDates' => $exchangeDatas,
            'dataHash' => md5(json_encode($exchangeDatas)),
        ]);

    }
}
