<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
//            $table->float('bid', 11, 6);
//            $table->float('ask', 11, 6);
//            $table->float('change', 11, 6);
//            $table->float('chanage_percentage', 11, 6);
            $table->float('open', 11, 6);
            $table->float('high', 11, 6);
            $table->float('low', 11, 6);
            $table->float('close', 11, 6);
            $table->datetime('datetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_datas');
    }
}
