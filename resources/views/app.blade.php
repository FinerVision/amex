<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="stylesheet" href="{{ debugCache('css/font-benton-sans.css') }}" />
    <link rel="stylesheet" href="{{ debugCache('css/app.css') }}" />
    <script type="text/javascript">
        !function() {
            var baseURI = '{{ $baseUri }}';
            var textContent = {!! json_encode($text) !!};
            window.text = function(key, values) {
                values = values || [];
                if (!Array.isArray(values)) { values = [values]; }
                var current = textContent;
                var sectors = key.split('.');
                sectors.map(function(sector) {
                    current = current[sector];
                });

                for(var i = 0; i < values.length; i++) {
                    current = current.replace('%d', values[i]);
                }

                return current;
            };

            window.requestAnimationFrame = window.requestAnimationFrame
                || window.mozRequestAnimationFrame
                || window.webkitRequestAnimationFrame
                || window.msRequestAnimationFrame
                || function(f){return setTimeout(f, 1000/60)};
        }();
    </script>
    <title>{{ $title }}</title>
</head>
<body>
<!--[if lte IE 8]>
<div class="ie8-notice">Unfortunately, we’re not able to fully support this tool on your web browser. To access this tool, please update your browser.</div>
<![endif]-->
<div id="app"
     data-currencies="{{ json_encode($currencies) }}"
     data-base-currency="{{ $baseCurrency }}"
     data-form-submit="{{ route('api.submit') }}"
     data-from="{{ json_encode($from) }}"
     data-to="{{ json_encode($to) }}"
     data-length-of-exposure="{{ json_encode($lengthOfExposure) }}"
     data-settings="{{ json_encode($settings) }}"
     data-current-exchange-rates="{{ json_encode($currentExchangeRates) }}"
></div>
<!--[if lte IE 9]><script src="{{ debugCache('js/typedarray.js') }}"></script><![endif]-->
<script src="{{ debugCache('js/app.js') }}"></script>
<?php
if($_ENV['GOOGLE_ANALYTICS_ID']) {
?>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', '<?php echo($_ENV['GOOGLE_ANALYTICS_ID']); ?>', 'auto');
    ga('send', 'pageview');

</script>
<?php
}
?>
</body>
</html>
