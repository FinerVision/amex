<!DOCTYPE html>
<html>
<head>
    <title>Calculations Behind the Screen</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.8.6/showdown.min.js"></script>
</head>
<body>

<div class="container">
    <div id="content"></div>
</div>

<script type="text/javascript">

    showdown.setFlavor('github');
    var converter = new showdown.Converter();
    var textBase64 = '{{ base64_encode($markdown) }}';
    var html = converter.makeHtml(atob(textBase64));

    document.querySelector('#content').innerHTML = html;

</script>
</body>
</html>
