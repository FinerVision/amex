const graphOptions = {
    width: 950,
    height: 500,
    lineWidth: 2,
    dashLineWidth: 4,
    pointsCircleRadius: 4,
    sidePadding: 60,
    bottomPadding: 60,
    chartTitleFontSize: 24,
    symbolFontSize: 18,
    dateFontSize: 14,
    dateOrdinalIndicatorFontSize: 10,
    gainAdjustmentBase: .8,
    shadowAlpha: .6,
    minMaxTopMargin: 10,
    color: {
        canvasBackground: '#ffffff',
        chartBackground: '#f4f4f4',
        primaryLineColor: '#373737',
        secondaryLineColor: '#9a9a9a',
        pathLineColor: '#6a6c6f',
        profitFillColor1: '#1799b7',
        profitFillColor2: '#0e6e92',
        lostFillColor1: '#1799b7',
        lostFillColor2: '#0e6e92',
        textColor: '#454545',
        dashLineColor: '#ba2a28',
        hedgedLineColor: '#ba2a28',
        hedgedLineColorUnhedged: '#6a6c6f',
    },
    defaultValues: {
        lineCap: 'round',
        lineWidth: 2,
        strokeStyle: '#373737',
    }
};

export default graphOptions;
