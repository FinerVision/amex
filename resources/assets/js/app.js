import React, {Component} from "react";
import {render} from "react-dom";
import anime from "animejs";
import browser from "detect-browser";

import Intro from "./Components/Intro";
import Form from "./Components/Form";
import Graph from "./Components/Graph";
import Percentage from "./Components/Percentage";
import Fluctuation from "./Components/Fluctuation";
import Difference from "./Components/Difference";
import Statement from "./Components/Statement";
import StepHint from "./Components/StepHint";
import MoreInformation from "./Components/MoreInformation";
import {scrollTo} from "./Helpers";

// detect browser
const html = document.documentElement;
const htmlClass = html.getAttribute('class');
html.setAttribute('class', `${htmlClass || ''} ${browser.name}`.trim());
html.setAttribute('data-browser-version', browser.version);

anime.easings['easeOut'] = t => 1 - Math.abs(Math.pow(t - 1, 3));

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showStatistics: false,
            graphData: [],
            fromCurrency: null,
            toCurrency: null,
            fromCurrencySymbol: null,
            toCurrencySymbol: null,
            graphCurrencySymbol: null,
            percentageHedged: props.settings.defaultPercentageHedged,
            startAmount: null,
            fluctuationAmount: null,
        };
    }

    setFluctuationAmount(fluctuationAmount) {
        if (fluctuationAmount !== this.state.fluctuationAmount) {
            this.setState({fluctuationAmount});
        }
    }

    onFormDataResponseSuccess(isFormValid, graphData, dataHash) {
        if(isFormValid) {
            const {fromCurrency, fromCurrencySymbol, toCurrencySymbol} = this.state;
            const startAmount = graphData[0] ? graphData[0].amount : null;
            this.setState({
                showStatistics: true,
                graphData,
                dataHash,
                startAmount,
                graphCurrencySymbol: fromCurrency === this.props.baseCurrency ? fromCurrencySymbol : toCurrencySymbol, // always get the base currency symbol
            });
        }

        // do the auto scroll
        this.autoScrollTo('stepHin2Top');
    }

    isFromForeignCurrency() {
        return this.props.baseCurrency === this.state.toCurrency;
    }

    updateFromCurrency(fromCurrencySymbol, fromCurrency) {
        const {baseCurrency} = this.props;
        if (fromCurrency !== baseCurrency) {
            this.updateBothCurrencies(fromCurrencySymbol, fromCurrency, this.props.currencies[baseCurrency].symbol, baseCurrency);
        } else {
            this.setState({fromCurrencySymbol, fromCurrency});
        }
    }

    updateToCurrency(toCurrencySymbol, toCurrency) {
        this.setState({toCurrencySymbol, toCurrency});
    }

    updateBothCurrencies(fromCurrencySymbol, fromCurrency, toCurrencySymbol, toCurrency, callback = () => {}) {
        this.setState({fromCurrencySymbol, fromCurrency, toCurrencySymbol, toCurrency}, callback);
    }

    switchCurrency(amount, from = this.state.fromCurrency, to = this.state.toCurrency) {
        return parseFloat(amount) * this.props.currentExchangeRates[`${from}${to}`];
    }

    switchBackCurrency(amount, from = this.state.fromCurrency, to = this.state.toCurrency) {
        return parseFloat(amount) / this.props.currentExchangeRates[`${from}${to}`];
    }

    autoScrollTo(ref) {
        const destinationElement = this.refs[ref];
        const container = this.refs.container;
        const {scrollTop} = container;
        const newScrollTop = scrollTop + destinationElement.getBoundingClientRect().top;

        scrollTo(container, newScrollTop);
    }

    render() {
        const {currencies, baseCurrency, formSubmit, from, to, lengthOfExposure, currentExchangeRates, settings} = this.props;
        const {showStatistics, graphData, dataHash, fromCurrencySymbol, toCurrencySymbol, fromCurrency, toCurrency, graphCurrencySymbol, percentageHedged, startAmount} = this.state;
        return (
            <div id="container" ref="container">
                <div id="content-wrapper">
                    <div className="custom-container">
                        <Intro />
                        <div className="container-inner">
                            <StepHint>
                                {text('stepHint.1')}
                            </StepHint>
                            <Form
                                currencies={currencies}
                                baseCurrency={baseCurrency}
                                formSubmit={formSubmit}
                                from={from}
                                to={to}
                                lengthOfExposure={lengthOfExposure}
                                highestVolatilitySwitch={settings.highestVolatilitySwitch}
                                currentExchangeRates={currentExchangeRates}
                                isFromForeignCurrency={this.isFromForeignCurrency.bind(this)}
                                fromCurrencySymbol={fromCurrencySymbol}
                                toCurrencySymbol={toCurrencySymbol}
                                fromCurrency={fromCurrency}
                                toCurrency={toCurrency}
                                updateBothCurrencies={this.updateBothCurrencies.bind(this)}
                                onFormDataResponseSuccess={this.onFormDataResponseSuccess.bind(this)}
                                showVolatilitySwitch={settings.showVolatilitySwitch}
                                switchBackCurrency={this.switchBackCurrency.bind(this)}
                            />
                            <div ref="stepHin2Top" />
                            <StepHint>
                                {text('stepHint.2')}
                            </StepHint>
                            <Fluctuation
                                fluctuationAmount={this.state.fluctuationAmount}
                                showStatistics={showStatistics}
                                data={graphData}
                                percentageHedged={percentageHedged}
                                graphCurrencySymbol={graphCurrencySymbol}
                                isFromForeignCurrency={this.isFromForeignCurrency.bind(this)}
                                switchBackCurrency={this.switchBackCurrency.bind(this)}
                            />
                            <Graph
                                showStatistics={showStatistics}
                                data={graphData}
                                dataHash={dataHash}
                                percentageHedged={percentageHedged}
                                graphCurrencySymbol={graphCurrencySymbol}
                                startAmount={startAmount}
                                showTodaysValues={settings.showTodaysValues}
                                rateReference={settings.rateReference}
                                fromCurrency={fromCurrency}
                                isFromForeignCurrency={this.isFromForeignCurrency.bind(this)}
                                switchBackCurrency={this.switchBackCurrency.bind(this)}
                                setFluctuationAmount={this.setFluctuationAmount.bind(this)}
                            />
                            <StepHint>
                                {text('stepHint.3')}
                            </StepHint>
                            <Percentage
                                showStatistics={showStatistics}
                                defaultPercentageHedged={settings.defaultPercentageHedged}
                                onPercentageHedgedChanged={percentageHedged => {
                                    const simplifiedPercentageHedged = percentageHedged.toFixed(2);
                                    if (simplifiedPercentageHedged !== parseFloat(this.state.percentageHedged).toFixed(2)) {
                                        this.setState({percentageHedged: parseFloat(simplifiedPercentageHedged)});
                                    }
                                }}
                            />
                            {settings.showTodaysValues && <Difference
                                showStatistics={showStatistics}
                                data={graphData}
                                percentageHedged={percentageHedged}
                                graphCurrencySymbol={graphCurrencySymbol}
                            />}
                            <StepHint>
                                {text('stepHint.4')}
                            </StepHint>
                            <MoreInformation
                                showStatistics={showStatistics}
                            />
                            <Statement />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const node = document.getElementById('app');

if(node !== null) {
    const props = {
        currencies: JSON.parse(node.getAttribute('data-currencies')),
        baseCurrency: node.getAttribute('data-base-currency'),
        formSubmit: node.getAttribute('data-form-submit'),
        from: JSON.parse(node.getAttribute('data-from')),
        to: JSON.parse(node.getAttribute('data-to')),
        lengthOfExposure: JSON.parse(node.getAttribute('data-length-of-exposure')),
        settings: JSON.parse(node.getAttribute('data-settings')),
        currentExchangeRates: JSON.parse(node.getAttribute('data-current-exchange-rates')),
    };
    render(<App {...props} />, node);
}
