import objectValues from "./objectValues";

// simple clone, it's not work for classes
const objectMap = (obj, callback) => {
    const keys = Object.keys(obj);
    const values = objectValues(obj);
    const result =  values.map((value, index) => callback(keys[index], value, index));
    const output = {};
    for(let i = 0; i < keys.length; i++) {
        output[keys[i]] = result[i];
    }
    return output;
};

export default objectMap;
