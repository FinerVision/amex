const objectValues = obj => {
    const keys = Object.keys(obj);
    const values = [];
    for(let i = 0; i < keys.length; i++) {
        const key = keys[i];
        values.push(obj[key]);
    }
    return values;
};

export default objectValues;
