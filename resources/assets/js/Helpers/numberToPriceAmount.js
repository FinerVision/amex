
const numberToPriceAmount = (num, currencySymbol, nonBreakingSpace = false) => {
    const numString = `${num}`;
    const numDecimal = numString.split('.');
    const numInt = numDecimal.shift();
    const numIntString = `${numInt}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return `${currencySymbol}${nonBreakingSpace ? ' ' : ''}${numIntString}.${numDecimal.join('')}`;
};

export default numberToPriceAmount;
