const onlyUnique = function(value, index, self) {
    return self.indexOf(value) === index;
}

const arrayUnique = arr => arr.filter(onlyUnique);

export default arrayUnique;