import browser from "detect-browser";

const browserMajor = () => parseInt(browser.version.split('.')[0]);

export default browserMajor;
