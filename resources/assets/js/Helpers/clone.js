// simple clone, it's not work for classes
const clone = obj => JSON.parse(JSON.stringify(obj));

export default clone;
