// simple clone, it's not work for classes
const arrayMax = (arr) => {
    let max = -Infinity;
    for(var i = 0; i < arr.length; i++) {
        if(arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
};

export default arrayMax;
