import swing from "./swing";

const DEFAULT_SCROLL_DURATION = 1000;
const DEFAULT_SCROLL_UPDATE_INTERVAL = 10;
const DEFAULT_SCROLL_UPDATE_TIMES = DEFAULT_SCROLL_DURATION / DEFAULT_SCROLL_UPDATE_INTERVAL;
const DEFAULT_SCROLL_PER_UPDATE = 1 / DEFAULT_SCROLL_UPDATE_TIMES;

const easingFunction = swing;

const scrollTo = (element, scrollTop) => {
    const {scrollTop: originalScrollTop} = element;
    const difference = scrollTop - originalScrollTop;
    let currentStep = 1;
    const scrollInterval = window.setInterval(() => {
        const scrollRatio = easingFunction(DEFAULT_SCROLL_PER_UPDATE * currentStep);

        element.scrollTop = originalScrollTop + scrollRatio * difference;

        // increase step
        currentStep++;

        // check if reached target
        if (currentStep > DEFAULT_SCROLL_UPDATE_TIMES) {
            window.clearInterval(scrollInterval);
        }

    }, DEFAULT_SCROLL_UPDATE_INTERVAL);
};

export default scrollTo;
