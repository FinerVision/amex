const isPointInCircle = (aX, aY, bX, bY, radius) => {
    // pythagorean theorem
    const a = aX - bX;
    const b = aY - bY;
    const aSquare = a * a;
    const bSquare = b * b;
    const c = Math.sqrt(aSquare + bSquare);
    return radius >= c;
};

export default isPointInCircle;