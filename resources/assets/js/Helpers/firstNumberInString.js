
const firstNumberInString = str => (str.match(/\d+\.\d+|\d+\.+|\d+\b|\d+(?=\w)/g) || []).shift();

export default firstNumberInString;
