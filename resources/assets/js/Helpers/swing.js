const swing = p => 0.5 - Math.cos( p * Math.PI ) / 2;

export default swing;
