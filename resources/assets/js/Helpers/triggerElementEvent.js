import browser from "detect-browser";

const triggerElementEvent = (element, eventName) => {
    try {
        if (browser.name === 'ie') {

            // for ie 11
            if (/^11./.test(browser.version)) {
                const event = document.createEvent('Event');
                event.initEvent(eventName, false, true);
                return element.dispatchEvent(event);
            } else {
                return element.fireEvent(new Event(eventName, {bubbles: true}));
            }
        }
        return element.dispatchEvent(new Event(eventName, {bubbles: false}));
    } catch(e) {
        // for ie browsers
        // just ignore it, make sure no exception is thrown
        console.error(e);
    }
};

export default triggerElementEvent;
