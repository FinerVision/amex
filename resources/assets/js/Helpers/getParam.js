const getParam = e => {e=e.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var n="[\\?&]"+e+"=([^&#]*)",r=new RegExp(n),c=r.exec(window.location.href);return null==c?"":c[1]};

export default getParam;
