const text = (key, values = []) => {
    let current = textContent;
    const sectors = key.split('.');
    sectors.map(sector => {
        current = current[sector];
    });

    for(let i = 0; i < values.length; i++) {
        current = current.replace('{{value}}', values[i]);
    }

    return current;
};

export default text;
