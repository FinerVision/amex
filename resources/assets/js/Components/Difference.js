import React, {Component} from "react";
import PropTypes from "prop-types";
import numberToPriceAmount from "../Helpers/numberToPriceAmount";
import IconTitle from "./IconTitle";

class Difference extends Component {
    calcDifference(data) {
        if(data.length === 0) {
            return null;
        }

        const {percentageHedged} = this.props;
        const percentageUnhedged = 1 - percentageHedged;
        const startAmount = data[0].amount;
        const endAmount = data[data.length - 1].amount;

        const currentRate = startAmount * percentageHedged + endAmount * percentageUnhedged;

        return Math.abs(currentRate - endAmount).toFixed(2);
    }

    render() {
        const {data, graphCurrencySymbol, showStatistics} = this.props;
        const difference = this.calcDifference(data);
        const differenceAmount = numberToPriceAmount(difference, graphCurrencySymbol);

        return (
            <div className={`Difference Section ${!showStatistics && 'hidden'}`}>
                <div className="Section__inner">
                    <IconTitle>
                        <div className="Difference__statement">
                            {text('difference.statement', differenceAmount)}
                        </div>
                    </IconTitle>
                    <div
                        className="Section__intro Difference__description"
                    >
                        {text('difference.description')}
                    </div>
                </div>
            </div>
        );
    }
}

export default Difference;

Difference.propTypes = {
    showStatistics: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
    percentageHedged: PropTypes.number,
    graphCurrencySymbol: PropTypes.string,
};

Difference.defaultValue = {
    percentageHedged: 0,
    graphCurrencySymbol: '',
};
