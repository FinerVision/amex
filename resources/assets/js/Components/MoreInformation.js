import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {getParam} from "../Helpers";

class MoreInformation extends Component {
    render() {
        return (
            <div className={classNames('Section', 'MoreInformation', {
                hidden: !this.props.showStatistics,
            })}>
                <div className="Section__inner MoreInformation__inner">
                    <div className="Section__intro MoreInformation__intro">
                        <p>{text('moreInformation.intro')}</p>
                        <p>
                            <a
                                className="MoreInformation__link-button"
                                href={text('moreInformation.submitFormButtonLink', [getParam('digi')])}
                                target="_blank"
                            >
                                {text('moreInformation.submitFormButtonText')}
                            </a>
                        </p>
                        <p>{text('moreInformation.bookmark')}</p>
                        <p>{text('moreInformation.thank')}</p>
                    </div>
                    <hr style={{ borderTop: '1px solid #666' }} />
                </div>
            </div>
        );
    }
}

MoreInformation.propTypes = {
    showStatistics: PropTypes.bool.isRequired,
}

export default MoreInformation;
