import React, {Component} from "react";

class Help extends Component {
    getHeader() {
        const {title} = this.props;
        return title ? (
            <div className="Help__header">
                {title}
            </div>
        ) : null;
    }

    render() {
        return (
            <div className="Help">
                <div className="Help__content">
                    {this.getHeader()}
                    <div className="Help__body">
                        {this.props.description}
                    </div>
                </div>
            </div>
        );
    }
}

export default Help;
