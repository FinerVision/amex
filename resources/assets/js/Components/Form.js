import React, {Component} from "react";
import request from "superagent";
import Help from "../Components/Help";
import {clone, objectMap, firstNumberInString, objectValues} from "../Helpers";

class Form extends Component {
    constructor(props) {
        super(props);

        this.currencies = objectValues(this.props.currencies);

        this.formValues = {
            from: '',
            to: '',
            totalAmount: '',
            lengthOfExposure: '',
            switchView: false
        };

        this.state = {
            fromValue: this.currencies[0].value,
            inputErrorMessage: {
                from: '',
                to: '',
                totalAmount: '',
                lengthOfExposure: '',
            },
            fromCurrencySymbol: this.props.currencies[this.props.from[0]].symbol,
            fromCurrency: this.props.from[0],
            toCurrencySymbol: this.props.currencies[this.props.to[1]].symbol,
            toCurrency: this.props.to[1],
        };

    }

    componentDidMount() {
        const formValues = {
            from: this.props.from[0] || '',
            to: this.props.to[1] || '',
            totalAmount: '',
            lengthOfExposure: this.props.lengthOfExposure[0].value,
            switchView: false,
        };
        this.formValues = formValues;
    }

    componentDidUpdate() {
        this.updateTotalAmount();
    }

    setInputErrorMessage(fields) {
        const inputErrorMessage = {
            from: fields.from || '',
            to: fields.to || '',
            totalAmount: fields.totalAmount || '',
            lengthOfExposure: fields.lengthOfExposure || '',
        };
        this.setState({inputErrorMessage});
    }

    resetInputErrorMessage() {
        this.setInputErrorMessage({});
    }

    getCurrencyFromSelect({name = "", onChange=() => {}}) {
        const {currencies} = this;

        return (
            <select ref="fromInput" name={name} onChange={event => {
                    const {value} = event.target;
                    const index = this.getCurrencyIndexByValue(value);
                    this.formValues[name] = value;
                    onChange(value, index);
                }}
            >
                {currencies.filter(item => this.props[name].indexOf(item.value) !== -1).map((item, index) => (
                    <option key={index} value={item.value}>{item.label}</option>
                ))}
            </select>
        );
    }

    getCurrencyToSelect({name = "", onChange=() => {}}) {
        const {currencies} = this;
        const {fromValue} = this.state;
        const {baseCurrency} = this.props;
        const options = currencies.filter(({value}) => fromValue === baseCurrency ^ value === baseCurrency);

        if (options.map(item => item.value).indexOf(this.formValues.to) === -1) {
            this.formValues.to = options[0].value;
        }

        return (
            <select ref="toInput" name={name} onChange={event => {
                const {value} = event.target;
                const index = this.getCurrencyIndexByValue(value);
                this.formValues[name] = value;
                onChange(value, index);
            }}>
                {options.filter(item => this.props[name].indexOf(item.value) !== -1).map((item, index) => (
                    <option key={index} value={item.value}>{item.label}</option>
                ))}
            </select>
        );
    }

    getLengthSelect({name = "", onChange=() => {}}) {
        const {lengthOfExposure} = this.props;
        return (
            <select name={name} onChange={event => {
                const {value} = event.target;
                this.formValues[name] = value;
                onChange(value);
            }}>
                {lengthOfExposure.map((item, index) => (
                    <option key={index} value={item.value}>{item.label}</option>
                ))}
            </select>
        );
    }

    getCurrencyIndexByValue(value) {
        const {currencies} = this;
        for(var i = 0; i < currencies.length; i++) {
            if (currencies[i].value === value) {
                return i;
            }
        }
        return null;
    }

    getCurrency(index) {
        const {currencies} = this;
        return currencies[index];
    }

    getCurrencySymbol(index) {
        this.getCurrency(index).symbol;
    }

    getForeignCurrencySymbolInCurrentPair() {
        const {baseCurrency} = this.props;
        const {from, to} = this.formValues;
        if (from === baseCurrency) {
            return this.state.toCurrencySymbol;
        } else if (to === baseCurrency) {
            return this.state.fromCurrencySymbol;
        }
        return null;
    }

    updateTotalAmount() {
        const {totalAmount} = this.refs;
        const value = firstNumberInString(totalAmount.value);
        totalAmount.value = `${this.getForeignCurrencySymbolInCurrentPair()}${value || ''}`;
        this.formValues.totalAmount = value;
    }

    totalAmountOnKeyPress(event) {
        // only accept minus and dot symbol and digits
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);
        if (!/(-|\.|[0-9])/.test(keyValue)) {
            event.preventDefault();
        }
    }

    getVolatitySwitch() {

        if (!this.props.showVolatilitySwitch) {
            return null;
        }

        return (
            <div className="Form__bottom text-center">
                <div className="Form__inline">
                    {text('form.switch.label', [this.props.highestVolatilitySwitch])}
                </div>
                &nbsp;&nbsp;
                <div className="Form__switch">
                    <input type="checkbox" onChange={event => {
                        const value = event.target.checked;
                        this.formValues.switchView = value;
                        this.submitForm();
                    }} />
                    <div className="Form__switch-visual" />
                </div>
            </div>
        );
    }

    submitForm() {
        const {onFormDataResponseSuccess, updateBothCurrencies, switchBackCurrency} = this.props;
        const {fromInput, toInput} = this.refs;


        const fromIndex = this.getCurrencyIndexByValue(fromInput.value);
        const {value: fromCurrency, symbol: fromCurrencySymbol} = this.getCurrency(fromIndex);
        const toIndex = this.getCurrencyIndexByValue(toInput.value);
        const {value: toCurrency, symbol: toCurrencySymbol} = this.getCurrency(toIndex);
        updateBothCurrencies(fromCurrencySymbol, fromCurrency, toCurrencySymbol, toCurrency, () => {

            const isFromForeignCurrency = this.props.isFromForeignCurrency();

            const formData = clone(this.formValues);
            formData.totalAmount = parseFloat(formData.totalAmount);

            // if this form is to any of foreign currencies
            if (!isFromForeignCurrency) {
                const {from, to} = formData;
                formData.totalAmount = switchBackCurrency(formData.totalAmount, from, to);
            }

            request
                .post(this.props.formSubmit)
                .send(formData)
                .end((err, res) => {

                        const data = JSON.parse(res.text);
                        const {invalidFields, success, chartDates, dataHash} = data;

                        if(typeof invalidFields == 'object' && Object.keys(invalidFields).length !== 0) {
                            const inputErrorMessage = objectMap(data.invalidFields, (key, value) => {
                                return value[0] || '';
                            });
                            this.setInputErrorMessage(inputErrorMessage);
                        }

                        if(!success) {
                            // TODO
                            return;
                        }

                        // reset the error message
                        this.resetInputErrorMessage();

                        //

                        // callback
                        if(onFormDataResponseSuccess) {
                            onFormDataResponseSuccess(success, chartDates, dataHash);
                        }

                    }
                )
        });
    }

    render() {
        return (
            <div className="Section Section--break-after Form">


                <div className="Section__inner Form__inner">

                    {/* intro */}
                    <div className="Section__intro Form__intro">
                        {text('form.intro')}
                    </div>

                    <div className="Form__table">

                        {/* row 1 */}
                        <div className="Form__row">
                            <div className="Form__group">
                                <div className="Form__label" data-error-message={this.state.inputErrorMessage.form}>
                                    {text('form.label.from')}
                                </div>
                                <div className="Form__select">
                                    {this.getCurrencyFromSelect({
                                        name: "from",
                                        onChange: value => {
                                            this.setState({fromValue: value});
                                            const {fromInput} = this.refs;
                                            const fromIndex = this.getCurrencyIndexByValue(fromInput.value);
                                            const {value: fromCurrency, symbol: fromCurrencySymbol} = this.getCurrency(fromIndex);
                                            this.setState({fromCurrencySymbol, fromCurrency});
                                        },
                                    })}
                                </div>
                            </div>
                            <div className="Form__group-separator" />
                            <div className="Form__group">
                                <div className="Form__label" data-error-message={this.state.inputErrorMessage.to}>
                                    {text('form.label.to')}
                                </div>
                                <div className="Form__select">
                                    {this.getCurrencyToSelect({
                                        name: "to",
                                        onChange: value => {
                                            const {toInput} = this.refs;
                                            const toIndex = this.getCurrencyIndexByValue(toInput.value);
                                            const {value: toCurrency, symbol: toCurrencySymbol} = this.getCurrency(toIndex);
                                            this.setState({toCurrencySymbol, toCurrency});
                                        },
                                    })}
                                </div>
                            </div>
                        </div>

                        {/* offset row */}
                        <div className="Form__row Form__row--offset" />

                        {/* row 2 */}
                        <div className="Form__row">
                            <div className="Form__group">
                                <div className="Form__label" data-error-message={this.state.inputErrorMessage.totalAmount}>
                                    {text('form.label.total_amount')}
                                </div>
                                <div className="Form__input">
                                    <input
                                        name="totalAmount"
                                        ref="totalAmount"
                                        onInput={this.updateTotalAmount.bind(this)}
                                        onKeyPress={this.totalAmountOnKeyPress.bind(this)}
                                        defaultValue={this.state.toCurrencySymbol}
                                    />
                                </div>
                            </div>
                            <div className="Form__group-separator" />
                            <div className="Form__group">
                                <div className="Form__label" data-error-message={this.state.inputErrorMessage.lengthOfExposure}>
                                    {text('form.label.length_of_exposure')}
                                    <Help
                                        title={text('form.help.length_of_exposure.title')}
                                        description={text('form.help.length_of_exposure.description')}
                                    />
                                </div>
                                <div className="Form__select">
                                    {this.getLengthSelect({
                                        name: 'lengthOfExposure',
                                        onChange: value => {

                                        }
                                    })}
                                </div>
                            </div>
                        </div>

                    </div>{/* .Form__table */}

                    <div className="Form__table Form__table--tight">
                        <div className="Form__row">
                            <div className="Form__group text-center">
                                <div className="Form__submit">
                                    <button
                                        onClick={() => this.submitForm()}
                                    >
                                        {text('form.submit')}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>{/* .Form__inner */}

                {this.getVolatitySwitch()}

            </div>
        );
    }
}

export default Form;
