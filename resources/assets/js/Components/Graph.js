import React, {Component} from "react";
import PropTypes from "prop-types";

import Canvas from "./Canvas";
import AbsoluteBanner from "./AbsoluteBanner";
import RelativeBanner from "./RelativeBanner";
import Help from "./Help";
import config from "../config";
import {numberToPriceAmount} from "../Helpers";

const {graphOptions} = config;

class Graph extends Component {
    constructor(props) {
        super(props);
        this.canvas = null;
        this.dataHash = '';
        this.state = {

            // details for banner when the cursor point to a point
            bannerText: '',
            bannerRate: 1,
            bannerTop: 0,
            bannerLeft: 0,

            // max value in the graph
            maxText: '',
            maxRate: 1,
            maxY: null,
            maxX: null,
            maxTop: null,
            maxLeft: null,

            // min value in the graph
            minText: '',
            minRate: 1,
            minY: null,
            minX: null,
            minTop: null,
            minLeft: null,

            // last value
            lastText: '',
            lastRate: 1,
            lastX: null,
            lastY: null,
            lastTop: null,
            lastLeft: null,

            // last value
            firstText: '',
            firstRate: 1,
            firstX: null,
            firstY: null,
            firstTop: null,
            firstLeft: null,

            startAmount: 0,
        };

        this.updateBannersPosition = this.updateBannersPosition.bind(this);
    }

    componentDidMount() {
        const canvasProps = {
            updateCurrentPointAmount: this.updateCurrentPointAmount.bind(this),
        };
        const canvas = new Canvas(this.refs.chartWrapper, graphOptions, canvasProps, this.props.rateReference);
        this.canvas = canvas;
        canvas.renderChart();

        window.addEventListener('resize', this.updateBannersPosition);

        // TODO : update the min max values here
        this.updateBannersValue();
    }

    componentDidUpdate() {
        const {data, dataHash, percentageHedged} = this.props;
        const {canvas} = this;

        if (this.dataHash !== dataHash && typeof data === 'object' && Object.keys(data)) {
            this.dataHash = dataHash;
            canvas.setData(data, this.forceUpdate.bind(this));
        }
        canvas.setPercentageHedged(percentageHedged);
        this.updateBannersValue();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateBannersValue);
    }

    /**
     * !!! IMPORTANT
     * This is where the state of this component be updated.
     */
    updateBannersValue() {
        const {maxText, maxRate, maxY, maxX, minText, minRate, minY, minX, lastText, lastRate, lastX, lastY, firstText, firstRate, firstX, firstY, startAmount} = this.state;
        const {canvas} = this;
        const max = canvas.getMax();
        const min = canvas.getMin();
        const last = canvas.getLast();
        const first = canvas.getFirst();
        const start = canvas.getFirstAmount();
        const newState = {};
        if (maxText !== max.amount) { newState.maxText = max.amount; }
        if (maxRate !== max.rate) { newState.maxRate = max.rate; }
        if (maxX !== max.x) { newState.maxX = max.x; }
        if (maxY !== max.y) { newState.maxY = max.y; }
        if (minText !== min.amount) { newState.minText = min.amount; }
        if (minRate !== min.rate) { newState.minRate = min.rate; }
        if (minX !== min.x) { newState.minX = min.x; }
        if (minY !== min.y) { newState.minY = min.y; }
        if (lastX !== last.x) { newState.lastX = last.x; }
        if (lastY !== last.y) { newState.lastY = last.y; }
        if (lastText !== last.amount) { newState.lastText = last.amount; }
        if (lastRate !== last.rate) { newState.lastRate = last.rate; }
        if (firstX !== first.x) { newState.firstX = first.x; }
        if (firstY !== first.y) { newState.firstY = first.y; }
        if (firstText !== first.amount) { newState.firstText = first.amount; }
        if (firstRate !== first.rate) { newState.firstRate = first.rate; }
        if (startAmount !== start) {newState.startAmount = start;}

        if (Object.keys(newState).length !== 0) {
            this.setState(newState);
        }
    }

    updateBannersPosition() {
        const {x: maxLeft, y: maxTop} = this.maxPosition();
        const {x: minLeft, y: minTop} = this.minPosition();
        const {x: lastLeft, y: lastTop} = this.lastPosition();

        this.setState({maxLeft, maxTop, minLeft, minTop, lastLeft, lastTop});
    }

    calcHedgedPosition({x, y}) {

        // canvas is not existing before the first render, do nothing
        if (this.canvas === null) {
            return {x: 0, y: 0};
        }

        const {percentageHedged} = this.props;
        const {top, middle} = this.canvas.getChartOffsets();
        const {canvas} = this.canvas; // yes, it's a canvas inside of a canvas
        const {top: rootTop, left: rootLeft} = this.refs.root.getBoundingClientRect();
        const {height: actualHeight, width: actualWidth, top: canvasTop, left: canvasLeft} = canvas.getBoundingClientRect();
        const {height: conceptualHeight, width: conceptualWidth} = canvas;
        const canvasWidthRatio = actualWidth / conceptualWidth;
        const canvasHeightRatio = actualHeight / conceptualHeight;

        const newX = canvasWidthRatio * x + (canvasLeft - rootLeft);
        const newY = (middle - (middle - y) * (1 - percentageHedged) + top) * canvasHeightRatio + (canvasTop - rootTop);
        return {x: newX, y: newY};
    }

    onMouseMoveChange(event) {

        const {pageX, pageY} = event;
        const {top, left} = this.refs.root.getBoundingClientRect();
        this.setState({
            bannerTop: pageY - top,
            bannerLeft: pageX - left,
        });
    }

    onMouseOutChange() {
        this.setState({
            bannerTop: 0,
            bannerLeft: 0,
        });
    }

    updateCurrentPointAmount(amount, rate) {
        const newState = {};
        newState.bannerText = amount === null ? null : this.calcHedgedAmount(amount);
        newState.bannerRate = rate;
        this.setState(newState);
    }

    calcHedgedAmount(amount) {
        const {percentageHedged, startAmount} = this.props;
        const percentageUnhedged = 1 - percentageHedged;
        const hedgedPart = percentageHedged * startAmount;
        const unhedgedPart = percentageUnhedged * amount;
        return hedgedPart + unhedgedPart;
    }

    maxPosition() {

        if (!this.canvas) { return {x: 0, y: 0}; }

        const {maxY, maxX} = this.state;

        if (maxY === null || maxX === null) { return {x: 0, y: 0}; }

        const {top: rootTop, left: rootLeft} = this.refs.root.getBoundingClientRect();
        const {top, left, width, height} = this.canvas.getBoundingClientRect();
        const canvasWidth = this.canvas.getCanvasWidth();
        const canvasHeight = this.canvas.getCanvasHeight();

        const x = left + (width / canvasWidth) * maxX - rootLeft;
        const y = top + (height / canvasHeight) * maxY - rootTop;
        return {x, y};
    }

    minPosition() {

        if (!this.canvas) { return {x: 0, y: 0}; }

        const {minY, minX} = this.state;

        if (minY === null || minX === null) { return {x: 0, y: 0}; }

        const {top: rootTop, left: rootLeft} = this.refs.root.getBoundingClientRect();
        const {top, left, width, height} = this.canvas.getBoundingClientRect();
        const canvasWidth = this.canvas.getCanvasWidth();
        const canvasHeight = this.canvas.getCanvasHeight();

        const x = left + (width / canvasWidth) * minX - rootLeft;
        const y = top + (height / canvasHeight) * minY - rootTop;
        return {x, y};
    }

    lastPosition() {

        if (!this.canvas) { return {x: 0, y: 0}; }

        const {lastY, lastX} = this.state;

        if (lastY === null || lastX === null) { return {x: 0, y: 0}; }

        const {top: rootTop, left: rootLeft} = this.refs.root.getBoundingClientRect();
        const {top, left, width, height} = this.canvas.getBoundingClientRect();
        const canvasWidth = this.canvas.getCanvasWidth();
        const canvasHeight = this.canvas.getCanvasHeight();

        const x = left + (width / canvasWidth) * lastX - rootLeft;
        const y = top + (height / canvasHeight) * lastY - rootTop;
        return {x, y};

    }

    customHedge(original, keeping, percentage) {
        return keeping * percentage + original * (1 - percentage);
    }

    render() {
        const {showStatistics, showTodaysValues, percentageHedged} = this.props;
        const {
            bannerText,
            bannerRate,
            bannerTop,
            bannerLeft,
            maxText,
            maxRate,
            maxY,
            maxX,
            minText,
            minRate,
            minY,
            minX,
            hedgedMaxText,
            hedgedMinText,
            lastY,
            lastX,
            lastText,
            lastRate,
            firstY,
            firstX,
            firstText,
            firstRate,
            startAmount,
        } = this.state;

        const showSwitchBack = !this.props.isFromForeignCurrency();
        const {graphCurrencySymbol} = this.props;

        const showBanner = !!(bannerText && bannerTop && bannerLeft);
        const showMax = !!(maxY !== null && maxX);
        const showMin = !!(minY !== null && minX);
        const showLast = !!(lastY !== null && lastX);
        const showHedgedMax = showMax;
        const showHedgedMin = showMin;
        const showStartAmount = !!(startAmount !== null && startAmount);

        const {x: maxLeft, y: maxTop} = this.maxPosition();
        const {x: minLeft, y: minTop} = this.minPosition();
        const {x: hedgedMaxLeft, y: hedgedMaxTop} = this.calcHedgedPosition({x: maxX, y: maxY});
        const {x: hedgedMinLeft, y: hedgedMinTop} = this.calcHedgedPosition({x: minX, y: minY});
        const {x: lastLeft, y: lastTop} = this.lastPosition();
        const {minMaxTopMargin} = config.graphOptions;

        let middleBannerText = '';
        let hoverBannerText = '';
        let maxBannerText = '';
        let minBannerText = '';
        let hedgedMaxBannerText = '';
        let hedgedMinBannerText = '';

        if (showSwitchBack) {
            middleBannerText = (lastText / firstRate).toFixed(2);
            hoverBannerText = parseFloat(this.customHedge(lastText * (1 / bannerRate), lastText / firstRate, this.props.percentageHedged)).toFixed(2);
            maxBannerText = (lastText * (1 / maxRate)).toFixed(2);
            minBannerText = (lastText * (1 / minRate)).toFixed(2);
            hedgedMaxBannerText = parseFloat(this.customHedge(lastText * (1 / maxRate), lastText / firstRate, this.props.percentageHedged)).toFixed(2);
            hedgedMinBannerText = parseFloat(this.customHedge(lastText * (1 / minRate), lastText / firstRate, this.props.percentageHedged)).toFixed(2);

            // hack
            window.setTimeout(() => this.props.setFluctuationAmount(Math.abs(lastText / maxRate - lastText / minRate)), 100);
        } else {
            middleBannerText = parseFloat(lastText / lastRate * firstRate).toFixed(2);
            hoverBannerText = parseFloat(this.customHedge(lastText * lastRate / bannerRate, lastText / lastRate * firstRate, this.props.percentageHedged)).toFixed(2);
            maxBannerText = (lastText * lastRate / maxRate).toFixed(2);
            minBannerText = (lastText * lastRate / minRate).toFixed(2);
            hedgedMaxBannerText = parseFloat(this.customHedge((lastText * lastRate / maxRate).toFixed(2), lastText / lastRate * firstRate, this.props.percentageHedged)).toFixed(2);
            hedgedMinBannerText = parseFloat(this.customHedge((lastText * lastRate / minRate).toFixed(2), lastText / lastRate * firstRate, this.props.percentageHedged)).toFixed(2);

            // hack
            window.setTimeout(() => this.props.setFluctuationAmount(Math.abs((lastText * lastRate / maxRate) - (lastText * lastRate / minRate))), 100);
        }

        return (
            <div
                ref="root"
                onMouseMove={this.onMouseMoveChange.bind(this)}
                onMouseOut={this.onMouseOutChange.bind(this)}
                className={`Section Graph ${showBanner ? 'Graph--show-banner' : ''} ${!showStatistics ? 'hidden' : ''}`}
            >
                <div className="Section__inner Graph__inner">
                    <div className="Section__title Graph__title">
                        {text('graph.title')}
                    </div>
                    <div className="row no-gutter">
                        <div className="col-md-7">
                            <div
                                className="Graph__chart"
                                ref="chartWrapper"
                            />
                        </div>
                        <div className="col-md-5">
                            <div className="Graph__legend">
                                <div className="Graph__legend-item">
                                    <div className="Graph__legend-title">
                                        {text('graph.legend.path_title')}
                                    </div>
                                    <div className="Graph__legend-image Graph__legend-image--path" />
                                </div>
                                <div className="Graph__legend-item">
                                    <div className="Graph__legend-title">
                                        {text('graph.legend.hedged_title')}
                                    </div>
                                    <div className="Graph__legend-image Graph__legend-image--hedged" />
                                </div>
                                <div className="Graph__legend-item">
                                    <div className="Graph__legend-title">
                                        {text('graph.legend.fixed_title')}
                                    </div>
                                    <div className="Graph__legend-image Graph__legend-image--fixed" />
                                </div>
                            </div>
                            <div className="Graph__legend-box">
                                {/** TODO : make break point utilities in individual file **/}
                                {/** no arrow in mobile **/}
                                <RelativeBanner
                                    arrowPosition={window.innerWidth >= 992 ? RelativeBanner.ARROW_LEFT : RelativeBanner.ARROW_NONE}
                                    dashed={true}
                                    show={showStartAmount}
                                >
                                    {numberToPriceAmount(middleBannerText, graphCurrencySymbol)}
                                </RelativeBanner>
                            </div>
                        </div>
                    </div>

                    {/** hover banner, shows when the user hovering it **/}
                    <AbsoluteBanner
                        top={bannerTop || 0}
                        left={bannerLeft || 0}
                        show={showBanner}
                    >
                        {numberToPriceAmount(hoverBannerText, graphCurrencySymbol)}
                    </AbsoluteBanner>

                    {/** max banner, shows at the maximum (highest point) of the graph **/}
                    <AbsoluteBanner
                        top={(Math.abs((maxTop - minMaxTopMargin) - (hedgedMaxTop - minMaxTopMargin)) > 50 ? (maxTop - minMaxTopMargin) : (hedgedMaxTop - minMaxTopMargin) - 50) || 0}
                        left={maxLeft || 0}
                        show={showMax}
                        hasTransparency={true}
                        grayScale={true}
                    >
                        {numberToPriceAmount(maxBannerText, graphCurrencySymbol)}
                        &nbsp;<Help
                            description={text('graph.bannerHelps.max')}
                        />
                    </AbsoluteBanner>

                    {/** min banner, shows at the minimum (highest point) of the graph **/}
                    <AbsoluteBanner
                        top={(Math.abs((minTop - minMaxTopMargin) - (hedgedMinTop - minMaxTopMargin)) > 42 ? (minTop - minMaxTopMargin) : (hedgedMinTop - minMaxTopMargin) + 42) || 0}
                        left={minLeft || 0}
                        show={showMin}
                        hasTransparency={true}
                        arrowPosition={AbsoluteBanner.ARROW_UNDER}
                        grayScale={true}
                    >
                        {numberToPriceAmount(minBannerText, graphCurrencySymbol)}
                        &nbsp;<Help
                            description={text('graph.bannerHelps.min')}
                        />
                    </AbsoluteBanner>

                    {/** hedged max banner, shows at the maximum (highest point) of the graph **/}
                    <AbsoluteBanner
                        top={hedgedMaxTop - minMaxTopMargin || 0}
                        left={hedgedMaxLeft || 0}
                        show={showHedgedMax}
                        positionAdjust={true}
                        grayScale={percentageHedged === 0}
                    >
                        {numberToPriceAmount(hedgedMaxBannerText, graphCurrencySymbol)}
                        &nbsp;<Help
                            description={text('graph.bannerHelps.maxHedged')}
                        />
                    </AbsoluteBanner>

                    {/** hedged min banner, shows at the minimum (highest point) of the graph **/}
                    <AbsoluteBanner
                        top={hedgedMinTop - minMaxTopMargin || 0}
                        left={hedgedMinLeft || 0}
                        show={showHedgedMin}
                        arrowPosition={AbsoluteBanner.ARROW_UNDER}
                        positionAdjust={true}
                        grayScale={percentageHedged === 0}
                    >
                        {numberToPriceAmount(hedgedMinBannerText, graphCurrencySymbol)}
                        &nbsp;<Help
                            description={text('graph.bannerHelps.minHedged')}
                        />
                    </AbsoluteBanner>

                    {/** last banner, basically this shows a hash (#) with a label of detail, shows at the last point of the graph **/}
                    <div
                        style={{
                            top: lastTop || 0,
                            left: lastLeft || 0,
                        }}
                        className={`Graph__hash ${showLast && showTodaysValues ? 'Graph__hash--show' : ''}`}
                    >
                        <div className="Graph__hash-detail Graph__hash-detail--under">
                            {text('graph.hashDetail')}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Graph;

Graph.propTypes = {
    data: PropTypes.array,
    percentageHedged: PropTypes.number,
    startAmount: PropTypes.number,
    graphCurrencySymbol: PropTypes.string,
    showStatistics: PropTypes.bool.isRequired,
    showTodaysValues: PropTypes.bool.isRequired,
    dataHash: PropTypes.string,
};

Graph.defaultValues = {
    data: [],
    percentageHedged: 0,
    startAmount: 0,
    dataHash: '',
};
