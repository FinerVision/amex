import React, {Component} from "react";
import ProgressBar from "../Components/ProgressBar";
import PropTypes from "prop-types";

class Percentage extends Component {
    render() {
        const {defaultPercentageHedged, onPercentageHedgedChanged, showStatistics} = this.props;
        return (
            <div className="Section Percentage">
                <div className={`Section__inner Percentage__inner ${!showStatistics && 'hidden'}`}>
                    <div className="Section__intro" dangerouslySetInnerHTML={{__html: text('percentage.intro')}} />
                    <div className="Percentage__content">
                        <div className="Percentage__slider">
                            <div className="Percentage__slider-label">
                                {text('percentage.sliderLabel')}
                            </div>
                            <div className="Percentage__slider-input">
                                <ProgressBar
                                    defaultValue={defaultPercentageHedged}
                                    onUpdate={progress => onPercentageHedgedChanged(progress)}
                                />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className="Section__intro" dangerouslySetInnerHTML={{__html: text('percentage.description')}} />
                </div>
            </div>
        );
    }
}

export default Percentage;

Percentage.propTypes = {
    onPercentageHedgedChanged: PropTypes.func.isRequired,
    showStatistics: PropTypes.bool.isRequired,
    defaultPercentageHedged: PropTypes.number.isRequired,
};
