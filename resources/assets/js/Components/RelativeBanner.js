import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

class RelativeBanner extends Component {
    render() {
        const {show, arrowPosition, dashed, children} = this.props;
        return (
            <div
                className={classNames(
                    'RelativeBanner',
                    {
                        'RelativeBanner--show': show,
                        'RelativeBanner--under': arrowPosition === RelativeBanner.ARROW_UNDER,
                        'RelativeBanner--left': arrowPosition === RelativeBanner.ARROW_LEFT,
                        'RelativeBanner--no-arrow': arrowPosition === RelativeBanner.ARROW_NONE,
                        'RelativeBanner--dashed': dashed,
                    }
                )}
            >
                {children}
            </div>
        );
    }
}

export default RelativeBanner;

RelativeBanner.ARROW_TOP = 'top';
RelativeBanner.ARROW_UNDER = 'under';
RelativeBanner.ARROW_LEFT = 'left';
RelativeBanner.ARROW_NONE = 'none';

RelativeBanner.propTypes = {
    show: PropTypes.bool,
    children: PropTypes.any.isRequired,
    arrowPosition: PropTypes.oneOf([RelativeBanner.ARROW_TOP, RelativeBanner.ARROW_UNDER, RelativeBanner.ARROW_LEFT, RelativeBanner.ARROW_NONE]),
};

RelativeBanner.defaultProps = {
    show: true,
    arrowPosition: RelativeBanner.ARROW_TOP,
};
