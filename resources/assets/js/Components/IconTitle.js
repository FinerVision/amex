import React, {Component} from "react";

class IconTitle extends Component {
    render() {
        return (
            <div className="IconTitle">
                <span className="IconTitle__inner">{this.props.children}</span>
            </div>
        )
    }
}

export default IconTitle;
