import React from "react";

const Statement = props => (
    <div className="Statement Section">
        <div className="Statement__content Section__inner" dangerouslySetInnerHTML={{__html: text('statement.content')}} />
        <div className="Statement__content Section__inner" dangerouslySetInnerHTML={{__html: text('statement.subcontent')}} />
    </div>
);

export default Statement;
