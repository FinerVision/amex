import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

class AbsoluteBanner extends Component {
    render() {
        const {top, left, show, hasTransparency, grayScale, arrowPosition, positionAdjust, children} = this.props;
        return (
            <div
                className={classNames(
                    'AbsoluteBanner',
                    {
                        'AbsoluteBanner--show': show,
                        'AbsoluteBanner--under': arrowPosition === AbsoluteBanner.ARROW_UNDER,
                        'AbsoluteBanner--adjust': positionAdjust,
                        'AbsoluteBanner--has-transparency': hasTransparency,
                        'AbsoluteBanner--gray-scale': grayScale,
                    }
                )}
                style={{top, left}}
            >
                <div className="AbsoluteBanner__inner">
                    {children}
                </div>
            </div>
        );
    }
}

export default AbsoluteBanner;

AbsoluteBanner.ARROW_TOP = 'top';
AbsoluteBanner.ARROW_UNDER = 'under';

AbsoluteBanner.propTypes = {
    top: PropTypes.number.isRequired,
    left: PropTypes.number.isRequired,
    show: PropTypes.bool.isRequired,
    children: PropTypes.any,
    hasTransparency: PropTypes.bool,
    arrowPosition: PropTypes.oneOf([AbsoluteBanner.ARROW_TOP, AbsoluteBanner.ARROW_UNDER]),
    positionAdjust: PropTypes.bool,
};

AbsoluteBanner.defaultProps = {
    children: null,
    hasTransparency: false,
    arrowPosition: AbsoluteBanner.ARROW_TOP,
    positionAdjust: false,
};
