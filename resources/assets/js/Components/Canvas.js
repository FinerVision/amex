import moment from "moment";
import anime from "animejs";
import browser from "detect-browser";
import {ordinal, clone, arrayMax, arrayUnique, isPointInCircle, rgba, browserMajor, noop} from "../Helpers";

class Canvas {
    constructor(node, options, props, rateReference) {
        const {width, height} = options;
        const canvas = document.createElement('canvas');

        canvas.width = width;
        canvas.height = height;

        node.appendChild(canvas);

        this.node = node;
        this.canvas = canvas;
        this.props = props;
        this.ctx = canvas.getContext('2d');
        this.options = options;
        this.data = [];
        this.percentageHedged = 0;
        this.waypoints = [];
        this.periodOfExposureMonths = [];
        this.waypointsGenerated = false;
        this.animationAdjustment = 0;
        this.animationObject = null;
        this.chartOffset = {};
        this.min = {};
        this.max = {};
        this.first = {};
        this.last = {};
        this.rateReference = rateReference;

        this.animation = {
            adjustment: 0
        };

        this.canvas.addEventListener('mousemove', this.mouseEventListener.bind(this));

    }

    mouseEventListener(event) {
        this.showHoveringPointRate(event);
    }

    showHoveringPointRate(event) {
        const {offsetX: mouseX, offsetY: mouseY} = event;
        const {canvas, waypoints} = this;
        const {pointsCircleRadius} = this.options;
        const canvasMouseX = (mouseX / canvas.offsetWidth) * canvas.width;
        const canvasMouseY = (mouseY / canvas.offsetHeight) * canvas.height;
        let hoveringPointAmount = null;
        let hoveringPointRate = null;

        for(let i = 0; i < waypoints.length; i++) {

            if (!this.shouldDrawPoints(i)) {
                continue;
            }

            const waypoint = waypoints[i];
            const {x, y, amount, rate} = waypoint;
            if (isPointInCircle(x, this.adjustY(y), canvasMouseX, canvasMouseY, pointsCircleRadius)) {
                // show the price here
                hoveringPointAmount = amount;
                hoveringPointRate = rate;
                break;
            }
        }
        this.props.updateCurrentPointAmount(hoveringPointAmount, hoveringPointRate);
    }

    showHedgedMaxPointRate() {

    }

    showHedgedMinPointRate() {

    }

    getPointAmount(amount, percentageHedged) {
        const percentageUnhedged = 1 - percentageHedged;
        const hedgedAmount = amount * percentageHedged;
        const unhedgedAmount = amount * percentageUnhedged;
        return unhedgedAmount + hedgedAmount;
    }

    clearCanvas() {
        const {ctx, canvas} = this;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }

    shouldDrawPoints(index) {
        switch (true) {
            case index === 0:
            case index === this.data.length - 1:
                return true;
            case this.displayingPoints.indexOf(index) === -1:
                return false;
            default:
                return true;
        }
    }

    renderBackground() {
        const {ctx} = this;
        const {lineWidth, color, width, height} = this.options;
        const {canvasBackground} = color;
        const {top} = this.getChartOffsets();

        ctx.fillStyle = canvasBackground;
        ctx.fillRect(lineWidth, top, width - (lineWidth * 2), height - (lineWidth * 4));
    }

    renderChartBackground() {
        const {ctx} = this;
        const {sidePadding, lineWidth, color, width} = this.options;
        const {chartBackground} = color;
        const {top, bottom} = this.getChartOffsets();

        // make chart background grey color
        ctx.fillStyle = chartBackground;
        ctx.fillRect(sidePadding + lineWidth, top, width - sidePadding * 2 - lineWidth * 2, bottom);
    }

    renderAxisBackground() {
        const {width, height, lineWidth, sidePadding, bottomPadding, color} = this.options;
        const waypoints = this.getWaypoints();
        const {primaryLineColor, secondaryLineColor} = color;
        const {top} = this.getChartOffsets();

        // Horizontal secondary Lines at the top
        this.renderLine(lineWidth + sidePadding, top, width - lineWidth - sidePadding, {
            orientation: 'horizontal',
            strokeStyle: '#dadada',
            lineWidth: 1
        });

        // Vertical secondary Lines
        const totalLines = 8;
        for (let i = 0; i < totalLines; i++) {
            const xOffset = ((((width - sidePadding * 2) - (lineWidth * 2)) / totalLines) * i) + lineWidth;

            this.renderLine(xOffset + sidePadding, top, height - (lineWidth * 2) - bottomPadding, {
                orientation: 'vertical',
                strokeStyle: secondaryLineColor,
                lineWidth: 1
            });
        }

        // Vertical Middle lines
        for (let i = 1; i < waypoints.length; i++) {

            if (i === waypoints.length - 1 || !waypoints[i].isEssentialDate) {
                continue;
            }

            const waypoint = waypoints[i];

            this.renderLine(waypoint.x - lineWidth / 2, lineWidth / 2, height + (lineWidth) - sidePadding, {
                orientation: 'vertical',
                strokeStyle: primaryLineColor,
            });
        }

        // Horizontal Bottom
        this.renderLine(lineWidth + sidePadding, height - bottomPadding - lineWidth * 2, width - lineWidth - sidePadding, {
            orientation: 'horizontal',
            strokeStyle: primaryLineColor,
        });

    }

    renderLine(x, y, length, options) {

        const {defaultValues} = this.options;

        options.lineCap = options.lineCap || defaultValues.lineCap;
        options.lineWidth = options.lineWidth || defaultValues.lineWidth;
        options.strokeStyle = options.strokeStyle || defaultValues.strokeStyle;

        const {ctx} = this;
        ctx.strokeStyle = options.strokeStyle;
        ctx.lineWidth = options.lineWidth;
        ctx.lineCap = options.lineCap;
        ctx.beginPath();
        ctx.moveTo(x, y);

        if (options.orientation === 'vertical') {
            ctx.lineTo(x, length);
        }

        if (options.orientation === 'horizontal') {
            ctx.lineTo(length, y);
        }

        ctx.stroke();
        ctx.closePath();
    }

    renderFixedPath() {
        const {ctx, data} = this;

        if (data.length === 0) {
            return;
        }
        // get points from the data
        const points = this.getWaypoints();

        // draw shapes
        this.drawDifferenceShapes(ctx, points, true);

        // draw lines
        this.drawLines(ctx, points, true);

        // make a circle at every points
        this.drawPoints(ctx, points, true);

        // make a cover for the whole graph so it looks like the behind one gets opacity
        this.opacityCover(ctx);
    }

    renderPath() {
        const {ctx, data} = this;

        if (data.length === 0) {
            return;
        }
        // get points from the data
        const points = this.getWaypoints();

        // draw shapes
        this.drawDifferenceShapes(ctx, points, false);

        // draw lines
        this.drawLines(ctx, points, false);

        // make a circle at every points
        this.drawPoints(ctx, points, false);
    }

    generateWaypoints(data) {
        if (data.length === 0) {
            return;
        }

        const {top, width, height, left} = this.getChartOffsets();
        const dataSpacing = width / (data.length - 1);
        const firstDateUnix = data[0].date;
        const lastDateUnix = data[data.length - 1].date;
        const graphRange = lastDateUnix - firstDateUnix;
        const waypoints = [];
        const displayingPoints = [];
        const {gainAdjustmentBase} = this.options;
        let isIncreasing = null;
        let min = {amount: Infinity, x: null, y: null};
        let max = {amount: -Infinity, x: null, y: null};

        // adjust the points value
        const maxValue = arrayMax(data.map(item => Math.abs(item.gain)));

        const gainAdjustment = gainAdjustmentBase / maxValue;

        for (let i = 0; i < data.length; i++) {

            const {unixtime, gain, amount, isEssentialDate} = data[i];
            const rate = data[i][this.rateReference];
            const x = left + (dataSpacing * i);
            const y = ((-gain * gainAdjustment + 1) / 2) * height + top;
            const nextGain = data[i + 1] ? data[i + 1].gain : null;
            const {month, day, year, ordinal} = this.getDateTexts(unixtime);
            const xPercentage = (unixtime - firstDateUnix) / graphRange;

            // generate min and max
            if (amount <= min.amount) {
                min.amount = amount;
                min.x = x;
                min.y = y;
                min.rate = rate;
            }

            if (amount >= max.amount) {
                max.amount = amount;
                max.x = x;
                max.y = y;
                max.rate = rate;
            }

            // generate first amount
            if (i === 0) {
                this.firstAmount = amount;
                this.first = {x, y, amount, rate};
            }

            // generate last
            if (i === data.length - 1) {
                this.last = {x, y, amount, rate};
            }

            if (isIncreasing === null) {
                isIncreasing = gain >= 0;
            }

            if (nextGain !== null && ((isIncreasing && gain > nextGain) || (!isIncreasing && gain < nextGain))) {
                displayingPoints.push(i);
                isIncreasing = !isIncreasing;
            }

            waypoints.push({x, y, month, year, day, ordinal, xPercentage, isEssentialDate, amount, rate});
            this.periodOfExposureMonths.push(month);
        }

        this.periodOfExposureMonths = arrayUnique(this.periodOfExposureMonths);

        this.displayingPoints = displayingPoints;
        this.waypoints = waypoints;
        this.waypointsGenerated = true;
        this.min = min;
        this.max = max;

    }

    getDateTexts(unixTime) {
        const formatDate = moment.unix(unixTime);
        return {
            month: formatDate.format('MMM'),
            day: formatDate.format('D'),
            year: formatDate.format('YYYY'),
            ordinal: ordinal(formatDate.format('D')),
        };
    }

    drawDifferenceShapes(ctx, points, isShadow) {
        const {left, right, middle} = this.getChartOffsets();
        const {displayingPoints} = this;
        points = clone(points).filter((value, index) => displayingPoints.indexOf(index) !== -1);

        let nextOppositeIndex = 0;
        let shapeEnded = true;
        let leftPoint, rightPoint = {}, topPoints;
        let isProfit = points[1].y <= middle;

        // fill the surface between the path and the middle line
        for (let i = 0; i < points.length; i++) {

            // if the point is still positive / negative as before, do some other things
            if (i >= nextOppositeIndex) {
                nextOppositeIndex = this.getNextOppositeIndex(points, i);
            }

            // initial
            if (i === 0) {
                topPoints = [];
                leftPoint = {};
                leftPoint.x = left;
                leftPoint.y = middle;
                shapeEnded = false;
            } else {
                // left point equals to last right point

                if (shapeEnded) {
                    leftPoint = rightPoint;
                    rightPoint = {};
                    topPoints = [];
                    shapeEnded = false;
                }
            }

            //
            if (nextOppositeIndex - 1 === i) {
                // calculate the end point
                const leftIndex = i;
                const rightIndex = nextOppositeIndex;
                const difference = points[rightIndex].y - points[leftIndex].y;
                const lengthPercentage = (middle - points[leftIndex].y) / difference;
                const endPointX = points[leftIndex].x + Math.abs(points[leftIndex].x - points[rightIndex].x) * lengthPercentage;

                rightPoint.x = endPointX;
                rightPoint.y = middle;

                shapeEnded = true;
            }

            else if (nextOppositeIndex - 1 === null) {
                rightPoint = {x: right, y: middle};
                shapeEnded = true;
            }

            topPoints.push(points[i]);

            if (i === points.length - 1) {

                topPoints.push(this.waypoints[this.waypoints.length - 1]);
                rightPoint = {
                    x: right,
                    y: middle
                };
                shapeEnded = true;
            }

            // draw
            if (shapeEnded) {

                const drawingPoints = [leftPoint, ...topPoints, rightPoint];
                ctx.beginPath();
                ctx.fillStyle = this.getFillGradientColor(drawingPoints);

                for (let i = 0; i < drawingPoints.length; i++) {
                    const {x, y} = drawingPoints[i];
                    const adjustedY = !isShadow ? this.adjustY(y) : this.adjustYWithoutPercentageHedged(y);
                    if (i === 0) {
                        ctx.moveTo(x, adjustedY);
                    } else {
                        ctx.lineTo(x, adjustedY);
                    }
                }

                ctx.fill();
                ctx.closePath();

                isProfit = !isProfit;

            }

        }
    }

    getFillGradientColor(drawingPoints) {

        const {middle} = this.getChartOffsets();
        let isProfit = false;

        for (let i = 0; i < drawingPoints.length; i++) {
            if (drawingPoints[i].y < middle) {
                isProfit = true;
            }
        }

        const {profitFillColor1, profitFillColor2, lostFillColor1, lostFillColor2} = this.options.color;
        let color1 = isProfit ? profitFillColor1 : lostFillColor1;
        let color2 = isProfit ? profitFillColor2 : lostFillColor2;

        // get the max and min point of the shape
        let max = 0;
        let min = Infinity;

        if (isProfit) {
            for (let i = 0; i < drawingPoints.length; i++) {
                const drawingPoint = drawingPoints[i];
                if (drawingPoint.y > max) {
                    max = drawingPoint.y;
                }
                if (drawingPoint.y < min) {
                    min = drawingPoint.y;
                }
            }
        } else {
            for (let i = 0; i < drawingPoints.length; i++) {
                const drawingPoint = drawingPoints[i];
                if (drawingPoint.x > max) {
                    max = drawingPoint.x;
                }
                if (drawingPoint.x < min) {
                    min = drawingPoint.x;
                }
            }
        }

        const {ctx} = this;
        const gradient = isProfit ? ctx.createLinearGradient(0, max, 0, min) : ctx.createLinearGradient(max, 0, min, 0);
        gradient.addColorStop(0, color1);
        gradient.addColorStop(1, color2);

        return gradient;
    }

    getNextOppositeIndex(arr, index) {
        const {middle} = this.getChartOffsets();
        const isProfit = arr[index].y >= middle; // let's assume 0 should be treated as positive number
        for (let i = index; i < arr.length; i++) {
            // TODO : is it just (!isProfit ^ arr[i] < 0)
            if ((isProfit && arr[i].y < middle) || (!isProfit && arr[i].y > middle)) {
                return i;
            }
        }
        return null;
    }

    drawLines(ctx, points, isShadow) {

        const {lineWidth} = this.options;
        const {pathLineColor, hedgedLineColor, hedgedLineColorUnhedged} = this.options.color;

        ctx.beginPath();
        ctx.lineJoin = 'bevel';
        for (let i = 0; i < points.length; i++) {

            if(!this.shouldDrawPoints(i)) {
                continue;
            }

            const {x, y} = points[i];

            const adjustedY = !isShadow ? this.adjustY(y) : this.adjustYWithoutPercentageHedged(y);

            if (i === 0) {
                ctx.moveTo(x, adjustedY);
                continue;
            }
            ctx.lineTo(x, adjustedY);
        }

        ctx.strokeStyle = isShadow ? pathLineColor : (this.percentageHedged !== 0 ? hedgedLineColor : hedgedLineColorUnhedged);
        ctx.lineWidth = lineWidth;
        ctx.stroke();
        ctx.closePath();
    }

    drawPoints(ctx, points, isShadow) {
        const {pointsCircleRadius, color} = this.options;
        const {pathLineColor, hedgedLineColor, hedgedLineColorUnhedged} = color;

        for (let i = 0; i < points.length; i++) {

            if (i === 0 || i === points.length - 1) {
                continue;
            }

            if(!this.shouldDrawPoints(i)) {
                continue;
            }

            const {x, y} = points[i];

            const adjustedY = !isShadow ? this.adjustY(y) : this.adjustYWithoutPercentageHedged(y);
            ctx.beginPath();
            ctx.arc(x, adjustedY, pointsCircleRadius, 0, 2 * Math.PI, false);
            ctx.fillStyle = isShadow ? pathLineColor : (this.percentageHedged !== 0 ? hedgedLineColor : hedgedLineColorUnhedged);
            ctx.fill();
            ctx.closePath();
        }
    }

    opacityCover(ctx) {
        const {top, bottom, left, right} = this.getChartOffsets();
        const {shadowAlpha} = this.options;
        const {canvasBackground} = this.options.color;
        const width = right - left;
        const height = bottom - top;

        ctx.fillStyle = rgba(canvasBackground, shadowAlpha);
        ctx.fillRect(left, top, width, height);
    }

    renderAxisFrame() {
        const {ctx} = this;
        const {sidePadding, lineWidth, dashLineWidth, width, height, primaryLineColor, color: {dashLineColor}} = this.options;
        const {left, right, middle} = this.getChartOffsets();

        // Vertical Left
        this.renderLine(lineWidth + sidePadding, lineWidth / 2, height + (lineWidth) - sidePadding, {
            orientation: 'vertical',
            strokeStyle: primaryLineColor,
        });
        // Vertical Right
        this.renderLine(width - lineWidth - sidePadding, lineWidth / 2, height + (lineWidth) - sidePadding, {
            orientation: 'vertical',
            strokeStyle: primaryLineColor,
        });

        // dashed line at the middle
        ctx.beginPath();
        ctx.strokeStyle = dashLineColor;
        ctx.lineCap = 'square';
        ctx.lineWidth = dashLineWidth;
        ctx.setLineDash && ctx.setLineDash([22, 22]);
        ctx.moveTo(left, middle);
        ctx.lineTo(right, middle);
        ctx.stroke();
        ctx.setLineDash && ctx.setLineDash([]);
        ctx.closePath();

    }

    renderText() {
        const {ctx} = this;
        const {chartTitleFontSize, symbolFontSize, color} = this.options;
        const {textColor} = color;

        const {top, left, bottom} = this.getChartOffsets();

        // font color
        ctx.fillStyle = textColor;

        // plus and minus symbol
        ctx.textAlign = 'left';
        ctx.font = `${symbolFontSize}px Arial`;
        ctx.textBaseline = 'alphabetic';
        ctx.fillText('+', left - chartTitleFontSize, top + (chartTitleFontSize / 2));
        ctx.fillText('-', left - chartTitleFontSize, bottom + (chartTitleFontSize / 2));

        // dates texts
        this.drawDateTexts(ctx);

    }

    getMonthsText(months) {
        if(months.length <= 3) {
            return months.join(', ');
        }
        return `${months[0]} to ${months[months.length - 1]}`;
    }

    drawDateTexts(ctx) {

        const {dateFontSize, dateOrdinalIndicatorFontSize, lineWidth, color} = this.options;
        const {textColor} = color;
        const {bottom} = this.getChartOffsets();
        const waypoints = this.getWaypoints();

        // font color
        ctx.fillStyle = textColor;

        for (let i = 0; i < waypoints.length; i++) {

            if (!waypoints[i].isEssentialDate) {
                continue;
            }

            const {x, day, month, year} = waypoints[i];

            // dates
            ctx.font = `${dateFontSize}px Arial`;
            ctx.textBaseline = 'top';
            ctx.textAlign = 'right';
            const datePositionY = bottom + dateFontSize + lineWidth * 2.5;
            const isSide = !!(i === 0 || i === waypoints.length - 1);
            ctx.fillText(isSide ? `${month} ${year}` : month, x + (isSide ? 30 : 15), datePositionY);

        }
    }


    countMonths() {
        return this.data.length;
    }

    getChartOffset(key) {
        const {width, height, sidePadding, bottomPadding, lineWidth} = this.options;
        switch (key) {
            case 'left':
                return this.chartOffset.left || ( this.chartOffset.left = sidePadding + lineWidth );
            case 'right':
                return this.chartOffset.right || ( this.chartOffset.right = width - (sidePadding + lineWidth) );
            case 'top':
                return this.chartOffset.top || ( this.chartOffset.top = lineWidth * 1.5 );
            case 'bottom':
                return this.chartOffset.bottom || ( this.chartOffset.bottom = height - bottomPadding - lineWidth * 3 );
            case 'width':
                return this.chartOffset.width || ( this.chartOffset.width = this.getChartOffset('right') - this.getChartOffset('left') );
            case 'height':
                return this.chartOffset.height || ( this.chartOffset.height = this.getChartOffset('bottom') - this.getChartOffset('top') );
            case 'center':
                return this.chartOffset.center || ( this.chartOffset.center = (this.getChartOffset('left') + this.getChartOffset('right')) / 2 );
            case 'middle':
                return this.chartOffset.middle || ( this.chartOffset.middle = (this.getChartOffset('top') + this.getChartOffset('bottom')) / 2 );
            default:
                return null;
        }
    }

    getChartOffsets() {
        return {
            left: this.getChartOffset('left'),
            right: this.getChartOffset('right'),
            top: this.getChartOffset('top'),
            bottom: this.getChartOffset('bottom'),
            width: this.getChartOffset('width'),
            height: this.getChartOffset('height'),
            center: this.getChartOffset('center'),
            middle: this.getChartOffset('middle'),
        };
    }

    // adjustment for animations and percentage hedged
    adjustY(y) {
        if (browser.name === 'ie' && browserMajor() <= 9) {
            return y;
        }
        const {middle} = this.getChartOffsets();
        return (y - middle) * (this.animationAdjustment * (1 - this.percentageHedged)) + middle;
    }

    // adjustment for animations only
    adjustYWithoutPercentageHedged(y) {
        if (browser.name === 'ie' && browserMajor() <= 9) {
            return y;
        }
        const {middle} = this.getChartOffsets();
        return (y - middle) * this.animationAdjustment + middle;
    }

    // getters and setters
    getWaypoints() {
        return clone(this.waypoints);
    }

    setWaypoints(waypoints) {
        this.waypoints = clone(waypoints);
    }

    getCanvasHeight() {
        return this.canvas.height;
    }

    getCanvasWidth() {
        return this.canvas.width;
    }

    getMin() {
        return this.min;
    }

    getMax() {
        return this.max;
    }

    getLast() {
        return this.last;
    }

    getFirst() {
        return this.first;
    }

    getFirstAmount() {
        return this.firstAmount;
    }

    getBoundingClientRect() {
        return this.canvas.getBoundingClientRect();
    }

    setData(data, onUpdateFinishCallback = noop) {

        // reset the attributes which should be generate after data's updating
        this.waypointsGenerated = false;
        this.animationAdjustment = 0;
        this.periodOfExposureMonths = [];

        if(!this.animationObject) {
            this.animationObject = anime({
                targets: this.animation,
                adjustment: 100,
                easing: 'easeOut',
                duration: 1000,
                delay: 300,
                round: 1,
                update: () => {
                    this.animationAdjustment = parseInt(this.animation.adjustment) / 100;
                },
                complete: () => onUpdateFinishCallback(),
            });
        } else {
            this.animationObject.restart();
        }

        this.data = data;
    }

    setPercentageHedged(percentageHedged) {
        this.percentageHedged = percentageHedged;
    }

    renderChart() {

        window.requestAnimationFrame(this.renderChart.bind(this));

        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        const {waypointsGenerated, data} = this;

        // clear background
        this.clearCanvas();

        // calculate the points (positions' x & y, month)
        if (!waypointsGenerated) {
            this.generateWaypoints(data);
        }

        // canvas background
        this.renderBackground();

        // chart background
        this.renderChartBackground();

        // fixed path, for comparison
        this.renderFixedPath();

        // axis background, the grey lines at the background
        this.renderAxisBackground();

        // path
        this.renderPath();

        // axis frame, the left and right path
        this.renderAxisFrame();

        // text in the chart
        this.renderText();

    }

}

export default Canvas;
