import React, {Component} from "react";

class StepHint extends Component {
    render() {
        return (
            <div className="StepHint">
                {this.props.children}
            </div>
        );
    }
}

export default StepHint;
