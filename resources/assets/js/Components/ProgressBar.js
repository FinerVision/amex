import React, {Component} from "react";
import PropTypes from "prop-types";

class ProgressBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            progress: props.defaultValue,
        };

        this.bindMethods = {
            onDotMouseUp: this.onDotMouseUp.bind(this),
            onDotMouseDown: this.onDotMouseDown.bind(this),
            onDotMouseEvent: this.onDotMouseEvent.bind(this),
        };

    }

    componentDidUpdate() {
        this.props.onUpdate(this.state.progress);
        document.body.addEventListener('mouseleave', this.bindMethods.onDotMouseUp);
    }

    getProgressText() {
        return `${Math.round(this.state.progress * 100)}%`;
    }

    onDotMouseDown(event) {
        this.bindMethods.onDotMouseEvent(event); // move the dot to where the mouse is when mouse down to anywhere of progress bar
        document.body.addEventListener('mousemove', this.bindMethods.onDotMouseEvent);
        document.body.addEventListener('touchmove', this.bindMethods.onDotMouseEvent);
        document.body.addEventListener('mouseup', this.bindMethods.onDotMouseUp);
        document.body.addEventListener('touchend', this.bindMethods.onDotMouseUp);
    }

    onDotMouseUp() {
        document.body.removeEventListener('mousemove', this.bindMethods.onDotMouseEvent);
        document.body.removeEventListener('touchmove', this.bindMethods.onDotMouseEvent);
        document.body.removeEventListener('mouseup', this.bindMethods.onDotMouseUp);
        document.body.removeEventListener('touchend', this.bindMethods.onDotMouseUp);
    }

    onDotMouseEvent(event) {
        const {root, progress} = this.refs;
        const progressLeft = progress.getBoundingClientRect().left;
        const barWidth = root.offsetWidth;

        let progressPixel = 0;

        switch(event.type) {
            case 'touchmove':
            case 'touchstart':
                progressPixel = event.touches[0].pageX - progressLeft;
                break;
            case 'mousemove':
            case 'mousedown':
                progressPixel = event.clientX - progressLeft;
        }

        let progressPercentage = progressPixel / barWidth;

        if (progressPercentage > 1) {
            progressPercentage = 1;
        } else if (progressPercentage < 0) {
            progressPercentage = 0;
        }

        this.setState({progress: parseFloat(progressPercentage.toFixed(2))});

    }

    render() {
        const {progress} = this.state;
        return (
            <div
                className="ProgressBar"
                ref="root"
                onMouseDown={this.bindMethods.onDotMouseDown}
                onTouchStart={this.bindMethods.onDotMouseDown}
            >
                <div
                    className="ProgressBar__progress"
                    ref="progress"
                    style={{
                        width: `${progress * 100}%`
                    }}
                />
                <div
                    className="ProgressBar__dot"
                >
                    <div className="ProgressBar__number text-center">
                        {this.getProgressText()}
                    </div>
                </div>
            </div>
        )
    }
}

export default ProgressBar;

ProgressBar.propTypes = {
    defaultValue: PropTypes.number.isRequired,
    onUpdate: PropTypes.func.isRequired,
};
