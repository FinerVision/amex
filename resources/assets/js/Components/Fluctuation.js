import React, {Component} from "react";
import PropTypes from "prop-types";

import numberToPriceAmount from "../Helpers/numberToPriceAmount";
import IconTitle from "./IconTitle";

class Fluctuation extends Component {

    render() {
        const {fluctuationAmount, graphCurrencySymbol, showStatistics} = this.props;

        if (typeof fluctuationAmount !== 'number') {
            return null;
        }
        const fluctuationAmountDisplay = numberToPriceAmount(fluctuationAmount.toFixed(2), graphCurrencySymbol);

        return (
            <div className="Fluctuation Section Section--break-container">
                <div className={`Section__inner ${!showStatistics && 'hidden'}`}>
                    <IconTitle>
                        <div
                            className="Fluctuation__detail"
                        >
                            {text('fluctuation.title', [fluctuationAmountDisplay])}
                        </div>
                    </IconTitle>
                    <div
                        className="Section__intro"
                    >
                        {text('fluctuation.description', [fluctuationAmountDisplay])}
                    </div>
                </div>
            </div>
        );
    }
}

export default Fluctuation;

Fluctuation.propTypes = {
    data: PropTypes.array.isRequired,
    graphCurrencySymbol: PropTypes.string,
    showStatistics: PropTypes.bool.isRequired,
};

Fluctuation.defaultValue = {
    graphCurrencySymbol: '',
};
