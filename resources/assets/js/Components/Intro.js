import React, {Component} from "react";

class Intro extends Component {
    render() {
        return (
            <div className="Intro Section Section--padding-bottom">
                <div className="Intro__inner Section__inner">
                    <div className="Intro__logo" />
                    <div className="Section__title Intro__title">
                        {text('intro.title')}
                    </div>
                    <div className="Intro__subtitle">
                        {text('intro.subtitle')}
                    </div>
                    <div className="Intro__brief">
                        {text('intro.brief')}
                    </div>
                </div>
            </div>
        );s
    }
}

export default Intro;
