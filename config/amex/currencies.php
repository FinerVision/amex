<?php

return [

    /**
     * base currency: the currency which is currently based on
     * this tool will treat all the other currencies as foreign currencies
     */
    'baseCurrency' => 'GBP',

    'from' => [
        'GBP',
        'EUR',
        'USD',
        'AUD',
        'CAD',
        'DKK',
        'HKD',
        'JPY',
        'NZD',
        'NOK',
        'SGD',
        'ZAR',
        'SEK',
        'CHF',
        'CZK',
        'MXN',
    ],
    'to' => [
        'GBP',
        'EUR',
        'USD',
        'AUD',
        'CAD',
        'DKK',
        'HKD',
        'JPY',
        'NZD',
        'NOK',
        'SGD',
        'ZAR',
        'SEK',
        'CHF',
        'CZK',
        'MXN',
    ],
    // currently just allowed the pairs from GBP or to GBP
    'availablePairs' => [
        'GBPEUR',
        'GBPUSD',
        'GBPAUD',
        'GBPCAD',
        'GBPDKK',
        'GBPHKD',
        'GBPJPY',
        'GBPNZD',
        'GBPNOK',
        'GBPSGD',
        'GBPZAR',
        'GBPSEK',
        'GBPCHF',
        'GBPCZK',
        'GBPMXN',

        'EURGBP',
        'USDGBP',
        'AUDGBP',
        'CADGBP',
        'DKKGBP',
        'HKDGBP',
        'JPYGBP',
        'NZDGBP',
        'NOKGBP',
        'SGDGBP',
        'ZARGBP',
        'SEKGBP',
        'CHFGBP',
        'CZKGBP',
        'MXNGBP',

    ],
    // the pairs in the availablePairs but can't be fetched in the API
    'calculatingPairs' => [
        'DKKGBP' => 'GBPDKK', // e.g. DKKGBP is not providing from the API, use the calculation from GBPDKK instead
        'HKDGBP' => 'GBPHKD',
        'SGDGBP' => 'GBPSGD',
    ],
    'details' => [
        'GBP' => [
            'label' => 'Pound Sterling (£)',
            'value' => 'GBP',
            'symbol' => '£',
        ],
        'EUR' => [
            'label' => 'Euro (€)',
            'value' => 'EUR',
            'symbol' => '€',
        ],
        'USD' => [
            'label' => 'US Dollar ($)',
            'value' => 'USD',
            'symbol' => '$',
        ],
        'AUD' => [
            'label' => 'Australian Dollar (A$)',
            'value' => 'AUD',
            'symbol' => 'A$',
        ],
        'CAD' => [
            'label' => 'Canadian Dollar (C$)',
            'value' => 'CAD',
            'symbol' => 'C$',
        ],
        'DKK' => [
            'label' => 'Danish Krone (kr.)',
            'value' => 'DKK',
            'symbol' => 'kr.',
        ],
        'HKD' => [
            'label' => 'Hong Kong Dollar (HK$)',
            'value' => 'HKD',
            'symbol' => 'HK$',
        ],
        'JPY' => [
            'label' => 'Japanese Yen (¥)',
            'value' => 'JPY',
            'symbol' => '¥',
        ],
        'NZD' => [
            'label' => 'New Zealand Dollar (NZ$)',
            'value' => 'NZD',
            'symbol' => 'NZ$',
        ],
        'NOK' => [
            'label' => 'Norwegian Krone (kr)',
            'value' => 'NOK',
            'symbol' => 'kr',
        ],
        'SGD' => [
            'label' => 'Singapore Dollar (S$)',
            'value' => 'SGD',
            'symbol' => 'S$',
        ],
        'ZAR' => [
            'label' => 'South African Rand (R)',
            'value' => 'ZAR',
            'symbol' => 'R',
        ],
        'SEK' => [
            'label' => 'Swedish Krona (kr)',
            'value' => 'SEK',
            'symbol' => 'kr',
        ],
        'CHF' => [
            'label' => 'Swiss Franc (Fr)',
            'value' => 'CHF',
            'symbol' => 'Fr',
        ],
        'MXN' => [
            'label' => 'Mexican Peso ($)',
            'value' => 'MXN',
            'symbol' => '$',
        ],
        'CZK' => [
            'label' => 'Czech Koruna (Kč)',
            'value' => 'CZK',
            'symbol' => 'Kč',
        ],
    ]
];
