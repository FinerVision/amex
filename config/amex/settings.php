<?php

return [

    'appTitle' => 'AMEX Microsite',

    /**
     * could be "open", "high", "low", or "close"
     */
    'rateReference' => 'open',

    /**
     * Switch view to the period of highest volatility (in the past ** months)
     */
    'highestVolatilitySwitch' => 12,

    /**
     * To show the volatility switch, true or false
     * (AMEX wanted to hide it temporarily)
     */
    'showVolatilitySwitch' => false,


    /**
     * Show the today's value's hash dot and the such section after the percentage hedged
     */
    'showTodaysValues' => false,

    /**
     * Default percentage hedged showing in the graph, this will affect how it shows in the
     * slider.
     * This value of this must be between 0 - 1 (inclusive)
     */
    'defaultPercentageHedged' => 1,

];
