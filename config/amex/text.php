<?php

return [
    'intro' => [
        'title' => 'Better manage your international payments',
        'subtitle' => 'Volatility is the name of the game but international payments don’t need to be a gamble.',
        'brief' => 'Try our Interactive benchmarking tool to discover how Forward Contracts can protect your business from currency market volatility. By securing a fixed exchange rate for international payments scheduled over the next year, your business can help protect margins while forecasting costs and profits with greater accuracy.',
    ],
    'stepHint' => [
        '1' => '1. Your payment needs',
        '2' => '2. Result',
        '3' => '3. Consider hedging as part of your risk management strategy',
        '4' => '4. Next steps',
    ],
    'form' => [
        'intro' => 'Think about an overseas invoice you need to pay or international payment you’re due to receive and then enter the relevant information about it below.',
        'label' => [
            'from' => 'From (base currency): ',
            'to' => 'To (destination currency): ',
            'total_amount' => 'Total amount: ',
            'length_of_exposure' => 'Payment due date: ',
        ],
        'help' => [
            'length_of_exposure' => [
                'title' => 'what’s this?',
                'description' => 'Payment due date refers to the specific date before / on which an invoice needs to be paid and may be a few weeks or months after an order is placed.'
            ],
        ],
        'submit' => 'Submit',
        'switch' => [
            'label' => 'Switch view to the period of highest volatility (in the past %d months)',
        ],
    ],
    'graph' => [
        'title' => 'Past currency volatility',
        'legend' => [
            'path_title' => 'Payment value using currency market exchange rate',
            'fixed_title' => 'Payment value using Forward Contract fixed exchange rate',
            'hedged_title' => 'Payment value at percentage hedged (see slider below)',
        ],
        'bannerHelps' => [
            'max' => 'This would be the value of your payment when the exchange rate was highest over the selected period (using historic data).',
            'min' => 'This would be the value of your payment when the exchange rate was lowest over the selected period (using historic data).',
            'maxHedged' => 'If you had secured a Forward Contract to cover all or part of your payment, this would have been the value of your payment when the exchange rate was highest, according to the percentage hedged slider below.',
            'minHedged' => 'If you had secured a Forward Contract to cover all or part of your payment, this would have been the value of your payment when the exchange rate was lowest, according to the percentage hedged slider below.',
        ],
        'hashDetail' => 'Today’s spot payment exchange rate could increase / decrease the value of your payment compared to a Forward Contract fixed rate ',
    ],
    'percentage' => [
        'intro' => '<p>Try using the slider below to see how hedging a percentage could affect the value of your payment in the chart above.</p>',
        'description' => '<p>Your business can use hedging tools e.g. Forward Contracts to cover the full value of your international payments, or just a percentage of them. By hedging a percentage of your payments, this protects your business from exchange rate volatility. For the balance of your payments that are not covered by a Forward Contract, you have the opportunity to use current exchange rates (spot market). This allows you to take on some risk, but limits it to whatever you’re comfortable with. You can even combine funds from a Forward Contract with those booked in the spot market by using our ‘blending’ functionality.</p>',
        'sliderLabel' => 'Percentage hedged slider',
    ],
    'fluctuation' => [
        'title' => 'Fluctuation: %d',
        'description' => 'When reviewing historic currency data from the period shown in the chart below, the value of your payment has changed by %d i.e. the difference between the value of your payment when the exchange rate was highest and when it was lowest. A fixed exchange rate offered through a Forward Contract can remove a business’s exposure to currency volatility altogether. It’s important to remember that even with a Forward Contract, your business may benefit less financially if market exchange rates become more favourable. But whatever happens, your currency risk is managed.',
    ],
    'difference' => [
        'statement' => 'Today’s difference in payment value (between market rate and Forward Contract fixed rate) = %d',
        'description' => 'This is the difference in the value of your payment using today’s exchange rate (via a spot payment) shown on the chart above and using a Forward Contracts fixed exchange rate, had you chosen to use one. By booking Forward Contracts beforehand, your business always has the choice to use it for a specific payment in case the current market rate isn’t favourable.'
    ],
    'moreInformation' => [
        'intro' => 'For more information or to book a Forward Contract, please call us on 0800 085 3456 or: ',
        'submitFormButtonText' => 'Submit an online enquiry form',
        'submitFormButtonLink' => 'https://corporateforms.americanexpress.com/internationalpayments?linknav=uk:fxip:15856:requestacalltop&CPID=7010O000001CTWU&digi=%d',
        'bookmark' => 'Please remember to bookmark this page so you can revisit the tool next time you are thinking about using currency hedging products.',
        'thank' => 'Thank you for using American Express\'s Forward Contracts Interactive Tool.',
    ],
    'statement' => [
        'content' => '<p>This tool should be used for information purposes only. It is intended to demonstrate how the historical performance of the selected currencies would have impacted an exposure of the specified amount, over the specified period. The currency pairs included in this tool represent those available for incoming and outgoing Forward Contracts which American Express currently offers. NetDania Creations ApS’s currency feed is the source of the data presented in this tool and incorporates Interbank tradable exchange rates taken from Tier-1 liquidity providers (major global banks). Information has been obtained from sources believed reliable and while every care has been taken in the preparation of the information, American Express makes no representation or warranties as to its accuracy or completeness. Past performance is not a reliable indicator of future performance. The tool does not take into account your particular objectives, financial situation or needs.  It should not be construed as financial or legal advice. You should consider its appropriateness in light of your circumstances and consider seeking professional advice relevant to your individual needs before making any financial decisions.</p><p>Please be aware that the following terms apply to American Express Forward Contracts: With our prior agreement, we may agree for you to purchase, and we may purchase from you, foreign currency at such an exchange rate and on such terms as we may agree for delivery on a specified future maturity date or dates or during a specified period, for a specified future payment need (a “Forward Exchange Contract”). We may require you to provide one or more advance payments or other collateral or security in a specified amount in respect of any Forward Exchange Contract. American Express may from time to time establish minimum and maximum transaction sizes and drawdown amounts for Forward Exchange Contracts. You would need to agree that you will use Forward Exchange Contracts in connection with your lawful future payment needs and shall not use Forward Exchange Contracts other than to facilitate a means of payment for identifiable goods and services. We shall be entitled to close-out, reverse or terminate a Forward Exchange Contract by any reasonable method and without prior notice if: i) you fail to pay any amount due and owing to American Express under the Agreement or are otherwise in default under the Agreement; ii) we reasonably believe that you will be unable to pay any such amount when due; or iii) the Agreement is terminated and such Forward Exchange Contracts have not reached their maturity date. In the event of any close-out, reversal or termination, you would need to agree to reimburse and indemnify any costs, expenses or losses we incur in closing-out, reversing or terminating the Forward Exchange Contract. You agree that we may apply any pre-payment or part-payment received in respect of any Forward Exchange Contract towards any such expenses or losses incurred by us and in the event of any shortfall, recover such further amounts by direct debit from your Designated Account (if applicable) or alternatively we reserve the right to invoice you separately or recover the funds by such other means.</p>',
        'subcontent' => '<p><a target="_blank" href="https://icm.aexp-static.com/Internet/eforms/International/GB_en/olaf/pdf/TermsConditions.pdf">Click here to access full UK FX International Payments Terms and Conditions</a></p>',
    ],
];
