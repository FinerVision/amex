<?php

return [
    [
        'label' => 'A Month',
        'value' => 1,
    ],
    [
        'label' => 'Two Months',
        'value' => 2,
    ],
    [
        'label' => 'Three Months',
        'value' => 3,
    ],
    [
        'label' => 'Four Months',
        'value' => 4,
    ],
    [
        'label' => 'Five Months',
        'value' => 5,
    ],
    [
        'label' => 'Six Months',
        'value' => 6,
    ],
    [
        'label' => 'Seven Months',
        'value' => 7,
    ],
    [
        'label' => 'Eight Months',
        'value' => 8,
    ],
    [
        'label' => 'Nine Months',
        'value' => 9,
    ],
    [
        'label' => 'Ten Months',
        'value' => 10,
    ],
    [
        'label' => 'Eleven Months',
        'value' => 11,
    ],
    [
        'label' => 'Twelve Months',
        'value' => 12,
    ],
];
