<?php

return [
    'real_time_quotes' => [
        'base_uri' => 'http://balancer.netdania.com',
        'path' => '/StreamingServer/StreamingServer',
        'params' => [
            'xml' => 'price',
            'group' => 'amex',
            'source' => 'netdania_rtc',
            'time' => 'YYYY-MM-dd HH:mm:ss',
            'tzone' => 'GMT',
        ]
    ],
    'historical_data' => [
        'base_uri' => 'http://balancer.netdania.com',
        'path' => '/StreamingServer/StreamingServer',
        'params' => [
            'xml' => 'chart',
            'group' => 'amex',
            'source' => 'netdania_rtc',
            'time' => 'YYYY-MM-dd HH:mm:ss',
            'max' =>'366',
            'scale' =>'1440', // a day
            'tzone' =>'GMT',
        ]
    ],
];
