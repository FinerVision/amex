const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir.config.css.autoprefix.options.browsers = ['> 1%', 'last 5 versions'];

elixir((mix) => {
    mix.sass('app.scss');
    mix.browserify('app.js');
    mix.browserify('typedarray.js');
});
